﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using HydraSQLite.Model.Lookups;
using HydraSQLite.Services;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Controllers.Api {

    [App.AuthoriseApi(Model.UserRole.Admin, Model.UserRole.Basic)]
    public class LookupApiController : ApiController {
        
        #region " dependencies "

        [Unity.Attributes.Dependency]
        public ILookupGroupRepository LookupGroupRepository { get; set; }

        #endregion

        // include hidden
        [AcceptVerbs("GET")]
        public IEnumerable<Item> AllLookupItems([FromUri]int id) {
            var lu = LookupGroupRepository.GetLookup((Group)id);
            return lu.Items;
        }

        // filter out hidden
        [AcceptVerbs("GET")]
        public IEnumerable<Item> LookupItems([FromUri]int id) {
            var lu = LookupGroupRepository.GetLookup((Group)id);
            return lu.Items.Where(itm => !itm.IsArchived);
        }

        [AcceptVerbs("POST")]
        public Item Save([FromBody]Item item, [FromUri] int id) {
            LookupGroupRepository.GetLookup((Group)id).Save(item);
            return item;
        }

        [AcceptVerbs("POST")]
        public Item Restore([FromBody]Item item, [FromUri] int id) {
            if (item.ID > 0 || item.IsArchived) {
                item.IsArchived = false;
                LookupGroupRepository.GetLookup((Group)id).Save(item);
            }
            return item;
        }

        [AcceptVerbs("POST")]
        public object Delete([FromBody]Item item, [FromUri] int id) {
            var error = "";
            var wasDeleted = false;
            try {
                wasDeleted = LookupGroupRepository.GetLookup((Group)id).Delete(item);
            }
            catch (System.Data.SqlClient.SqlException) {
                error = "delete failed";
            }
            return new {
                message = string.IsNullOrEmpty(error) ? "ok" : "error", wasDeleted, error
            };
        }
    }
}
