﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using HydraSQLite.App;
using HydraSQLite.Model;
using HydraSQLite.Model.Api;
using HydraSQLite.Services;
using HydraSQLite.Services.BusinessLogic;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Controllers.Api {
    
    public class UserApiController : ApiController {

        #region " dependencies "

        [Unity.Attributes.Dependency]
        public IBreezeConfiguration Config { get; set; }

        [Unity.Attributes.Dependency]
        public ILogin Login { get; set; }

        [Unity.Attributes.Dependency]
        public ILookupGroupRepository LookupGroupRepository { get; set; }

        [Unity.Attributes.Dependency]
        public IUserRepository UserRepository { get; set; }

        [Unity.Attributes.Dependency]
        public ICompanyRepository CompanyRepository { get; set; }

        [Unity.Attributes.Dependency]
        public IUserLogic UserLogic { get; set; }

        [Unity.Attributes.Dependency]
        public IPasswordLogic PasswordLogic { get; set; }

        [Unity.Attributes.Dependency]
        public IEmailer Emailer { get; set; }

        #endregion

        [AcceptVerbs("GET"), AuthoriseApi(UserRole.Basic)]
        public object GetLookups() {
            return new {
                roles = typeof(UserRole).ToLookups<UserRole>(),
                statuses = typeof(UserStatus).ToLookups<UserStatus>()
            };
        }

        [AcceptVerbs("GET"), AuthoriseApi(UserRole.LoggedOut)]
        public PasswordLogic.CheckResult ValidatePassword(string password) {
            return PasswordLogic.Check(password);
        }
        
        // Anyone can call this - but it will only return non-zero for admin users
        [AcceptVerbs("GET"), AuthoriseApi(UserRole.Basic)]
        public object GetAwaitingApprovalCount() {
            var user = Login.CurrentUser;
            var count =  (user == null || user.Role != UserRole.Admin) ? 0 : UserRepository.GetAwaitingApprovalCount();
            return new {
                count
            };
        }
        
        [AcceptVerbs("POST"), AuthoriseApi(UserRole.Admin)]
        public IEnumerable<User> Search(UserSearchParameters searchParms) {
            return UserRepository.Search(searchParms);
        }

        [AcceptVerbs("GET"), AuthoriseApi(UserRole.Basic)]
        public User Load(Guid id) {
            return UserRepository.Load(id);
        }

        [AcceptVerbs("POST"), AuthoriseApi(UserRole.Admin)]
        public async Task<object> Save(User user) {
            var existing = UserRepository.Load(user.ID);
            await UserLogic.PreSave(Emailer, existing, user);
            if (UserRepository.EmailIsInUse(user)) {
                return new { message = "error", error = "The email " + user.Email + " is already in use."};
            }
            if (!string.IsNullOrEmpty(user.NewPassword)) {
                var newPasswordCheck = PasswordLogic.Check(user.NewPassword);
                if (!newPasswordCheck.IsOkay) {
                    return new {message = "error", error = newPasswordCheck.Message};
                }
                user.Password = Config.IsDigestAuthenticationScheme ? 
                    PasswordStorageDigest.Create(user.NewPassword) : 
                    PasswordStorageSecure.Create(user.NewPassword);
            }
            UserRepository.Save(user);           
            return new {message = "ok", error = ""};
        }

        [AcceptVerbs("POST"), AuthoriseApi(UserRole.Admin)]
        public object Delete(User user) {
            UserRepository.Delete(user);
            return new {
                message = "ok"
            };
        }
        
        [AcceptVerbs("GET"), AuthoriseApi(UserRole.Basic)]
        public User LoggedInUser() {
            return Login.CurrentUser;
        }

        [AcceptVerbs("POST")]
        public object ChangePassword(PasswordChange passwordChange) {
            var user = Login.CurrentUser;
            var isOldPasswordOkay = Config.IsDigestAuthenticationScheme ? 
                PasswordStorageDigest.VerifyPassword(passwordChange.Password, user.Password): 
                PasswordStorageSecure.VerifyPassword(passwordChange.Password, user.Password);
            if (!isOldPasswordOkay) {
                return new { message = "error", error = "Incorrect Current Password" };
            }

            var newPasswordCheck = PasswordLogic.Check(passwordChange.NewPassword);
            if (!newPasswordCheck.IsOkay) {
                return new { message = "error", error = newPasswordCheck.Message };
            }

            user.Password = Config.IsDigestAuthenticationScheme ? 
                PasswordStorageDigest.Create(passwordChange.NewPassword) : 
                PasswordStorageSecure.Create(passwordChange.NewPassword);
            UserRepository.Save(user);
            return new { message = "ok", error = "" };
        }
    }
}