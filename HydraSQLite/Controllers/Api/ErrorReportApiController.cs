﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HydraSQLite.App;

namespace HydraSQLite.Controllers.Api {

    /// <summary>
    /// An API endpoint so that javascript errors can be reported
    /// </summary>
    public class ErrorReportApiController : ApiController {

        private readonly IBreezeConfiguration myConfig;

        public ErrorReportApiController(IBreezeConfiguration config) {
            myConfig = config;
        }

        [AcceptVerbs("POST")]
        public HttpResponseMessage PostErrorReport(Model.ErrorReport report) {
            if (Utils.IsDebug()) return Request.CreateResponse(HttpStatusCode.OK, new {
                message = "Debug mode - no error sent."
            });

            report.Occurred = DateTime.Now;
            report.Application = myConfig.BasiliskAppName;
            try {
                var req = WebRequest.Create(myConfig.BasiliskUrl);
                req.Method = "POST";
                req.ContentType = "text/json";
                using (var writer = new System.IO.StreamWriter(req.GetRequestStream())) {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(report);
                    writer.Write(json);
                    writer.Close();
                }
                // ReSharper disable AssignNullToNotNullAttribute
                req.BeginGetResponse(null, null);
                return Request.CreateResponse(HttpStatusCode.OK, new {
                    message = "ok"
                });
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new {
                    message = "did not successfully send error report"
                });
            }
        }
    }
}
