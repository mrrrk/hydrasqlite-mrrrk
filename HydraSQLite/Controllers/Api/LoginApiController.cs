﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HydraSQLite.App;
using HydraSQLite.Model.Api;
using HydraSQLite.Services;
using HydraSQLite.Services.BusinessLogic;
using HydraSQLite.Services.Mockable;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Controllers.Api {

    [AuthoriseApi(Model.UserRole.LoggedOut)]
    public class LoginApiController : ApiController {


        #region " dependencies "

        [Unity.Attributes.Dependency]
        public IBreezeConfiguration Config { get; set; }

        [Unity.Attributes.Dependency]
        public ICache Cache { get; set; }
        
        [Unity.Attributes.Dependency]
        public IEmailer Emailer { get; set; }

        [Unity.Attributes.Dependency]
        public ILogin Login { get; set; }

        [Unity.Attributes.Dependency]
        public IEmailUrlKey EmailUrlKey { get; set; }

        [Unity.Attributes.Dependency]
        public IUserRepository UserRepository { get; set; }

        [Unity.Attributes.Dependency]
        public IPasswordLogic PasswordLogic { get; set; }

        #endregion

        //private readonly Login Login;
        //private readonly ICache Cache;
        //private readonly Services.BusinessLogic.PasswordLogic myPasswordBusinessLogic;

        //public LoginApiController(
        //    IBreezeConfiguration config,
        //    IEmailer emailer,
        //    ICache cache, 
        //    IEmailUrlKey emailUrlKey, 
        //    IUserRepository userRepo) {

        //    Login = new Login(config, emailer, cache, emailUrlKey, userRepo);
        //    Cache = cache;
        //    myPasswordBusinessLogic = new Services.BusinessLogic.PasswordLogic(config, cache);
        //}

        [System.Web.Http.AcceptVerbs("GET")]
        public object CheckAuthentication() {
            //Debug.WriteLine("is auth:" + User.Identity.IsAuthenticated.ToString());
            return new {
                isAuthenticated = User.Identity.IsAuthenticated
            };
        }

        #region " login "
        
        [System.Web.Http.AcceptVerbs("GET")]
        public object ServerNOnce(string user) {
            return new {
                message = "ok",
                serverNOnce = Login.CreateServerNOnce(user)
            };
        }

        [System.Web.Http.AcceptVerbs("POST")]
        public async Task<LoginResponse> DigestLogin(LoginRequest req) {
            if (Request.RequestUri.Scheme == Uri.UriSchemeHttps && !Utils.IsDebug()) {
                throw new InvalidOperationException("Don't use DigestLogin if secure HTTP - Use SecureLogin instead.");
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var urlHelperAdaptor = new UrlHelperMvcAdaptor(urlHelper);
            var formsAuthentication = new FormsAuthenticationWrapper();
            var response = Login.CheckDigest(urlHelperAdaptor, formsAuthentication, req);
            await AddDelay("login", req.Email, response.IsSuccess);
            return response;
        }

        [System.Web.Http.AcceptVerbs("POST")]
        public async Task<LoginResponse> SecureLogin(LoginRequest req) {
            if (Request.RequestUri.Scheme != Uri.UriSchemeHttps && !Utils.IsDebug()) {
                throw new InvalidOperationException("SecureLogin requires secure HTTP! - Use DigestLogin instead or put this on a secure server.");
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var urlHelperAdaptor = new UrlHelperMvcAdaptor(urlHelper);
            var formsAuthentication = new FormsAuthenticationWrapper();
            var response = Login.CheckEmailAndPassword(urlHelperAdaptor, formsAuthentication, req);
            await AddDelay("login", req.Email, response.IsSuccess);
            return response;
        }

        #endregion

        #region " forgot - reminder "

        [System.Web.Http.AcceptVerbs("POST")]
        public async Task<ForgotResponse> Forgot(ForgotRequest req) {
            var urlHelper = Url;
            return await Login.Forgot(urlHelper, req);
        }

        [System.Web.Http.AcceptVerbs("POST")]
        public ResetPassword ResetPasswordInit(ResetPassword resetPassword) {
            Login.ResetPasswordInit(resetPassword);
            return resetPassword;
        }

        [System.Web.Http.AcceptVerbs("POST")]
        public ResetPassword ResetPassword(ResetPassword resetPassword) {
            Login.ResetPassword(resetPassword, PasswordLogic);
            return resetPassword;
        }
        
        #endregion

        // use async so that we don't tie up threads in request thread pool while we're delaying (?)
        private async Task AddDelay(string keyPrefix, string email, bool isSuccess) {
            // key the attempts on IP address so that hack attempts don't necessarily lock the real user out.
            var key = $"{keyPrefix}-{email}-{(GetClientIp() ?? "")}";
            if (isSuccess) {
                Cache.Remove(key);
                return;
            }
            var triesSoFar = (int)(Cache.Get(key) ?? 0);
            var delayMillis = triesSoFar == 0 ? 0 : (int)Math.Pow(2, triesSoFar) * 300;
            // limit to 10 secs
            delayMillis = delayMillis < 10000 ? delayMillis : 10000;
            Cache.Set(key, ++triesSoFar, DateTime.Now + TimeSpan.FromMinutes(2));
            await Task.Delay(delayMillis);           
        }

        private string GetClientIp(HttpRequestMessage request = null) {
            request = request ?? Request;
            if (request.Properties.ContainsKey("MS_HttpContext")) {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            if (HttpContext.Current != null) {
                return HttpContext.Current.Request.UserHostAddress;
            }
            return null;           
        }

    }
}