﻿using System.Web.Mvc;

namespace HydraSQLite.Controllers.Mvc {

    [App.AuthoriseMvc(Model.UserRole.Basic)]
    public class HomeController : Controller {

        public ActionResult Index() {
            return View();
        }

        public ActionResult MyDetails() {
            return View();
        }
    }
}