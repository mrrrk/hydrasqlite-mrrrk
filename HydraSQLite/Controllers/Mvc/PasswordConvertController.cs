﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using HydraSQLite.Services;

namespace HydraSQLite.Controllers.Mvc {

    // Used for converting encrypted passwords (for digest login) to proper hashed passwords
    public class PasswordConvertController : ApiController {
        
        private readonly IUserRepository myUserRepo;

        public PasswordConvertController(IUserRepository userRepo) {
            myUserRepo = userRepo;
        }

        private const string Key = "a168kje5ffgnod2mx8oj3xk2gzukvmbwuwigmerb";

        [AcceptVerbs("GET"), App.AuthoriseMvc(Model.UserRole.LoggedOut)]
        public HttpResponseMessage Run(string key) {
            if (key != Key) { 
                return GeneratePage("<p>Invalid key.</p>");
            }
            var log = new StringBuilder();
            log.AppendLine("<pre>");
            PasswordStorageDigest.ConvertPasswords(log, myUserRepo);
            log.AppendLine("</pre>");
            return GeneratePage(log.ToString());
        }

        private HttpResponseMessage GeneratePage(string content) {
            var html = $@"<!DOCTYPE html>
<html>
    <head>
        <title>Password Convert</title>
        <style>
            body {{ font-family: arial, helvetica; margin:50px; }}
            h2 {{ font-style: italic; }}
            pre {{ margin-top:20px; padding:20px; border:1px solid #ccc; background: #eee; }}
        </style>
    </head>
    <body>
        <h2>Password Convertor</h2>
        {content}
    </body>
</html>
";
            var response = new HttpResponseMessage(HttpStatusCode.OK) {
                Content = new StringContent(html)
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }
    }
}