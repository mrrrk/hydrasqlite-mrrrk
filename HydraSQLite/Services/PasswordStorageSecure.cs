﻿using System;
using System.Security.Cryptography;

//https://github.com/defuse/password-hashing
namespace HydraSQLite.Services {

    internal class InvalidHashException : Exception {
        public InvalidHashException() { }
        public InvalidHashException(string message)
            : base(message) { }
        public InvalidHashException(string message, Exception inner)
            : base(message, inner) { }
    }

    internal class CannotPerformOperationException : Exception {
        public CannotPerformOperationException() { }
        public CannotPerformOperationException(string message)
            : base(message) { }
        public CannotPerformOperationException(string message, Exception inner)
            : base(message, inner) { }
    }

    static class PasswordStorageSecure {
        // These constants may be changed without breaking existing hashes.
        public const int NumberOfSaltBytes = 24;
        public const int NumberOfHashBytes = 18;
        public const int NumberOfPbkdf2Iterations = 64000;

        // These constants define the encoding and may not be changed.
        public const int NumberOfHashFields = 5;
        public const int HashAlgorithmFieldIndex = 0;
        public const int IterationCountFieldIndex = 1;
        public const int HashSizeFieldIndex = 2;
        public const int SaltFieldIndex = 3;
        public const int Pbkdf2FieldIndex = 4;

        public static string Create(string password) {
            // Generate a random salt
            var salt = new byte[NumberOfSaltBytes];
            try {
                using (var csprng = new RNGCryptoServiceProvider()) {
                    csprng.GetBytes(salt);
                }
            }
            catch (CryptographicException ex) {
                throw new CannotPerformOperationException(
                    "Random number generator not available.",
                    ex
                );
            }
            catch (ArgumentNullException ex) {
                throw new CannotPerformOperationException(
                    "Invalid argument given to random number generator.",
                    ex
                );
            }

            var hash = Pbkdf2(password, salt, NumberOfPbkdf2Iterations, NumberOfHashBytes);

            // format: algorithm:iterations:hashSize:salt:hash
            var parts = "sha1:" +
                NumberOfPbkdf2Iterations +
                ":" +
                hash.Length +
                ":" +
                Convert.ToBase64String(salt) +
                ":" +
                Convert.ToBase64String(hash);
            return parts;
        }

        public static bool VerifyPassword(string password, string goodHash) {
            char[] delimiter = { ':' };
            var split = goodHash.Split(delimiter);

            if (split.Length != NumberOfHashFields) {
                throw new InvalidHashException(
                    "Fields are missing from the password hash.  Are you using the right login scheme?"
                );
            }

            // We only support SHA1 with C#.
            if (split[HashAlgorithmFieldIndex] != "sha1") {
                throw new CannotPerformOperationException(
                    "Unsupported hash type."
                );
            }

            int iterations;
            try {
                iterations = int.Parse(split[IterationCountFieldIndex]);
            }
            catch (ArgumentNullException ex) {
                throw new CannotPerformOperationException(
                    "Invalid argument given to int.Parse",
                    ex
                );
            }
            catch (FormatException ex) {
                throw new InvalidHashException(
                    "Could not parse the iteration count as an integer.",
                    ex
                );
            }
            catch (OverflowException ex) {
                throw new InvalidHashException(
                    "The iteration count is too large to be represented.",
                    ex
                );
            }

            if (iterations < 1) {
                throw new InvalidHashException(
                    "Invalid number of iterations. Must be >= 1."
                );
            }

            byte[] salt;
            try {
                salt = Convert.FromBase64String(split[SaltFieldIndex]);
            }
            catch (ArgumentNullException ex) {
                throw new CannotPerformOperationException(
                    "Invalid argument given to Convert.FromBase64String",
                    ex
                );
            }
            catch (FormatException ex) {
                throw new InvalidHashException(
                    "Base64 decoding of salt failed.",
                    ex
                );
            }

            byte[] hash;
            try {
                hash = Convert.FromBase64String(split[Pbkdf2FieldIndex]);
            }
            catch (ArgumentNullException ex) {
                throw new CannotPerformOperationException(
                    "Invalid argument given to Convert.FromBase64String",
                    ex
                );
            }
            catch (FormatException ex) {
                throw new InvalidHashException(
                    "Base64 decoding of pbkdf2 output failed.",
                    ex
                );
            }

            int storedHashSize;
            try {
                storedHashSize = int.Parse(split[HashSizeFieldIndex]);
            }
            catch (ArgumentNullException ex) {
                throw new CannotPerformOperationException(
                    "Invalid argument given to int.Parse",
                    ex
                );
            }
            catch (FormatException ex) {
                throw new InvalidHashException(
                    "Could not parse the hash size as an integer.",
                    ex
                );
            }
            catch (OverflowException ex) {
                throw new InvalidHashException(
                    "The hash size is too large to be represented.",
                    ex
                );
            }

            if (storedHashSize != hash.Length) {
                throw new InvalidHashException(
                    "Hash length doesn't match stored hash length."
                );
            }

            var testHash = Pbkdf2(password, salt, iterations, hash.Length);
            return SlowEquals(hash, testHash);
        }

        private static bool SlowEquals(byte[] a, byte[] b) {
            var diff = (uint)a.Length ^ (uint)b.Length;
            for (var i = 0; i < a.Length && i < b.Length; i++) {
                diff |= (uint)(a[i] ^ b[i]);
            }
            return diff == 0;
        }

        private static byte[] Pbkdf2(string password, byte[] salt, int iterations, int outputBytes) {
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt)) {
                pbkdf2.IterationCount = iterations;
                return pbkdf2.GetBytes(outputBytes);
            }
        }
    }
}
