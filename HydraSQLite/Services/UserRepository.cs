﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HydraSQLite.Model;
using HydraSQLite.App;
using HydraSQLite.Model.Api;
using HydraSQLite.Services.Data;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Services {

    public interface IUserRepository {
        
        int GetAwaitingApprovalCount();
        
        User Load(Guid id);

        IList<User> Search(UserSearchParameters parameters);

        bool EmailIsInUse(User user);

        void Delete(User user);

        void Save(User user);
    }

    public class UserRepository : IUserRepository {

        #region " dependencies "

        [Unity.Attributes.Dependency]
        public IBreezeConfiguration Config { get; set; }

        #endregion

        public int GetAwaitingApprovalCount() {
            return new FluentData(Config)
                .SqlText($"SELECT COUNT(usrID) AS count FROM tblUser WHERE usrStatus = {(int)UserStatus.WaitingApproval};")
                .ExecuteReader(reader => FluentData.NonNullValue<int>(reader["count"]))
                .FirstOrDefault();
        }

        public IList<User> Search(UserSearchParameters parameters) {
            return new FluentData(Config)
                .SqlText(SearchSQL(parameters))
                .AddParameter("@Email", parameters.Email.ToLower()) // SQLite is case sensitive
                .AddParameter("@Text", parameters.Text.ToLower())
                // note - status IDs added via SQL string (less cumbersome than doing via parameter)
                .ExecuteReader(Load);
        }

        public bool EmailIsInUse(User user) {
            if (user == null) return false;
            return new FluentData(Config)
                .SqlText("SELECT COUNT(*) AS count FROM tblUser WHERE usrEmail = @Email and usrID <> @ID;")
                .AddParameter("@ID", user.ID.ToString("B"))
                .AddParameter("@Email", user.Email)
                .ExecuteReader(reader => FluentData.NonNullValue<int>(reader["count"]))
                .FirstOrDefault() > 0;
        }

        public User Load(Guid id) {
            return new FluentData(Config)
                .SqlText("SELECT * FROM tblUser WHERE usrID = @ID;")
                .AddParameter("@ID", id.ToString("B"))
                .ExecuteResult(Load);
        }

        public void Delete(User user) {
            try {
                // user ID might be used in audit fields, etc. and might throw foreign key exception
                new FluentData(Config)
                    .SqlText("DELETE FROM tblUser WHERE usrID = @ID;")
                    .AddParameter("@ID", user.ID.ToString("B"))
                    .ExecuteNonQuery();
            }
            catch (SqlException ex) {
                if (!SqlErrors.IsForeignKeyConstraintError(ex)) {
                    throw;
                }
                user.Status = UserStatus.Deleted;
                Save(user);
            }
        }

        public void Save(User user) {
            const string sql = @"
INSERT OR REPLACE INTO tblUser (
        usrID,
        usrRole,
        usrStatus,
        usrEmail,
        usrForename,
        usrSurname,
        usrRegistrationNotes,
        usrPassword,
        usrLastLoggedIn,
        usrPrevLastLoggedIn,
        usrCreated,
        usrCreatedByUserID,
        usrLastUpdated,
        usrLastUpdatedByUserID,
        usrDeleted,
        usrDeletedByUserID
    )
    VALUES (
        @ID,
        @Role,
        @Status,
        @Email,
        @Forename,
        @Surname,
        @RegistrationNotes,
        @Password,
        @LastLoggedIn,
        @PrevLastLoggedIn,
        @Created,
        @CreatedByUserID,
        @LastUpdated,
        @LastUpdatedByUserID,
        @Deleted,
        @DeletedByUserID
    );";
            new FluentData(Config)
                .SqlText(sql)
                .AddParameter("@ID", DbType.String, user.ID.ToString("B"))
                .AddParameter("@Role", DbType.Int32, (int)user.Role)
                .AddParameter("@Status", DbType.Int32, (int)user.Status)
                .AddParameter("@Email", DbType.String, user.Email)
                .AddParameter("@Forename", DbType.String, user.Forename)
                .AddParameter("@Surname", DbType.String, user.Surname)
                .AddParameter("@RegistrationNotes", DbType.String, user.RegistrationNotes)
                .AddParameter("@Password", DbType.String, user.Password)
                .AddParameter("@LastLoggedIn", DbType.DateTime, user.LastLoggedIn.ToDatabaseValue())
                .AddParameter("@PrevLastLoggedIn", DbType.DateTime, user.PrevLastLoggedIn.ToDatabaseValue())
                .AddParameter("@Created", DbType.DateTime, user.Created.ToDatabaseValue())
                .AddParameter("@CreatedByUserID", DbType.String, user.CreatedByUserID.ToString("B"))
                .AddParameter("@LastUpdated", DbType.DateTime, user.LastUpdated.ToDatabaseValue())
                .AddParameter("@LastUpdatedByUserID", DbType.String, user.LastUpdatedByUserID.ToString("B"))
                .AddParameter("@Deleted", DbType.DateTime, user.Deleted.ToDatabaseValue())
                .AddParameter("@DeletedByUserID", DbType.String, user.DeletedByUserID.ToString("B"))
                .ExecuteNonQuery();
        }

        private string SearchSQL(UserSearchParameters searchParms) {

            var sql = "SELECT DISTINCT tblUser.* FROM tblUser";

            sql += "\nWHERE 1 = 1";

            if (!string.IsNullOrEmpty(searchParms.Email)) {
                sql += "\nAND usrEmail = @email";
            }
            if (!string.IsNullOrEmpty(searchParms.Text)) {
                sql += "\nAND (usrForename + ' ' + usrSurname LIKE '%' || @text || '%' OR usrEmail LIKE '%' || @text || '%')";
            }
            // if no statuses selected, just return all by default
            if (searchParms.Statuses.Length == 1) {
                sql += $"\nAND usrStatus = {(int)searchParms.Statuses[0]}";
            }
            else if (searchParms.Statuses.Length > 1) {
                sql += $"\nAND usrStatus in ({string.Join(", ", searchParms.Statuses.Select(s => (int)s)) })";
            }
            sql += "\nORDER BY usrSurname, usrForename;";
            return sql;
        }

        private User Load(IDataReader reader) {
            return new User {
                ID = FluentData.NonNullValue<Guid>(reader["usrID"]),
                Role = (UserRole)FluentData.NonNullValue<int>(reader["usrRole"]),
                Status = (UserStatus)FluentData.NonNullValue<int>(reader["usrStatus"]),
                Email = FluentData.NonNullValue<string>(reader["usrEmail"]),
                Forename = FluentData.NonNullValue<string>(reader["usrForename"]),
                Surname = FluentData.NonNullValue<string>(reader["usrSurname"]),
                RegistrationNotes = FluentData.NonNullValue<string>(reader["usrRegistrationNotes"]),
                Password = FluentData.NonNullValue<string>(reader["usrPassword"]),
                LastLoggedIn = FluentData.NonNullValue<DateTime>(reader["usrLastLoggedIn"]),
                PrevLastLoggedIn = FluentData.NonNullValue<DateTime>(reader["usrPrevLastLoggedIn"]),
                Created = FluentData.NonNullValue<DateTime>(reader["usrCreated"]),
                CreatedByUserID = FluentData.NonNullValue<Guid>(reader["usrCreatedByUserID"]),
                LastUpdated = FluentData.NonNullValue<DateTime>(reader["usrLastUpdated"]),
                LastUpdatedByUserID = FluentData.NonNullValue<Guid>(reader["usrLastUpdatedByUserID"]),
                Deleted = FluentData.NonNullValue<DateTime>(reader["usrDeleted"]),
                DeletedByUserID = FluentData.NonNullValue<Guid>(reader["usrDeletedByUserID"])
            };
        }
    }
}
