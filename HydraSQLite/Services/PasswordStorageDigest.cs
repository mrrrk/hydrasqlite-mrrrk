﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using HydraSQLite.Model.Api;

namespace HydraSQLite.Services {

    static class PasswordStorageDigest {

        private static readonly string myKey = "RxvUPzuQGP3ktf9LKZbaYgnWGHH0ziiEv3uiXMfuqqpTHIlrzXdApZs2s0PmbN7bdtonlF9t05qc7j9GZQdufzXQ00nmRPKBqeVl";
        private static readonly byte[] mySalt = Encoding.ASCII.GetBytes("Yj0QLxiGyQGdCj");
        private static readonly string myPrefix = "digest:";

        private class InvalidEncryptedPassword : Exception {
            public InvalidEncryptedPassword(string message)
                : base(message) { }
        }

        public static string Create(string plainText) {

            if (string.IsNullOrEmpty(plainText)) throw new ArgumentNullException(nameof(plainText));

            string encryptedText;
            RijndaelManaged aesAlg = null;
            try {
                var key = new Rfc2898DeriveBytes(myKey, mySalt);
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (var msEncrypt = new MemoryStream()) {
                    // prepend the IV
                    msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)) {
                        using (var swEncrypt = new StreamWriter(csEncrypt)) {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                    }
                    encryptedText = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally {
                // Clear the RijndaelManaged object.
                aesAlg?.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return myPrefix + encryptedText;
        }

        // this is for password change process - until (and if) can think of better approach
        public static bool VerifyPassword(string password, string encryptedPassword) {
            return password == DecryptPassword(encryptedPassword);
        }

        // for digest login
        public static bool VerifyPassword(
            string digest, 
            string serverNOnce, 
            string clientNOnce, 
            string encryptedPassword) {

            return digest == Crypto.GetHash(serverNOnce + clientNOnce + DecryptPassword(encryptedPassword));
        }

        public static bool IsEncryptedPassword(string encryptedPassword) {
            return !string.IsNullOrWhiteSpace(encryptedPassword) && encryptedPassword.StartsWith(myPrefix);
        }

        private static string DecryptPassword(string cipherText) {

            if (string.IsNullOrEmpty(cipherText))
                throw new ArgumentNullException(nameof(cipherText));

            // check and remove prefix
            if(!cipherText.StartsWith(myPrefix))
                throw new InvalidEncryptedPassword("Incorrect prefix");
            cipherText = cipherText.Substring(myPrefix.Length, cipherText.Length - myPrefix.Length);

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext;

            try {
                // generate the key from the shared secret and the salt
                var key = new Rfc2898DeriveBytes(myKey, mySalt);

                // Create the streams used for decryption.                
                var bytes = Convert.FromBase64String(cipherText);
                using (var msDecrypt = new MemoryStream(bytes)) {
                    // Create a RijndaelManaged object
                    // with the specified key and IV.
                    aesAlg = new RijndaelManaged();
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    // Get the initialization vector from the encrypted stream
                    aesAlg.IV = ReadByteArray(msDecrypt);
                    // Create a decrytor to perform the stream transform.
                    var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)) {
                        using (var srDecrypt = new StreamReader(csDecrypt))

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            finally {
                // Clear the RijndaelManaged object.
                aesAlg?.Clear();
            }

            return plaintext;
        }


        public static void ConvertPasswords(StringBuilder log, IUserRepository userRepo) {
            var usersFound = 0;
            var digestPasswordsFound = 0;
            var passwordsIgnored = 0;
            var errors = new List<string>();
            var digestPasswordsConverted = 0;

            // get everyone
            var users = userRepo.Search(new UserSearchParameters());
            foreach (var user in users) {
                usersFound++;
                if (!IsEncryptedPassword(user.Password)) {
                    passwordsIgnored++;
                    continue;
                }
                try {
                    var plain = DecryptPassword(user.Password);
                    digestPasswordsFound++;
                    //Debug.WriteLine($"{user.Email} : {plain} : {user.Password}");
                    user.Password = PasswordStorageSecure.Create(plain);
                    userRepo.Save(user);
                    digestPasswordsConverted++;
                }
                catch (InvalidEncryptedPassword) {
                    passwordsIgnored++;
                }
                catch (Exception e) {
                    errors.Add(e.Message);
                }
            }

            log.AppendLine($"Users found                   :{usersFound}");
            log.AppendLine($"Digest passwords found        :{digestPasswordsFound}");
            log.AppendLine($"Non-digest passwords ignored  :{passwordsIgnored}");
            log.AppendLine($"Errors                        :{errors.Count}");
            log.AppendLine($"Digest passwords converted    :{digestPasswordsConverted}");

            if (errors.Any()) {
                log.AppendLine();
                log.AppendLine();
                log.AppendLine("ERRRORS:");
                log.AppendLine();
            }

            foreach (var error in errors) {
                log.AppendLine($"    {error}");
            }
        }

        private static byte[] ReadByteArray(Stream s) {
            var rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length) {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            var buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length) {
                throw new SystemException("Did not read byte array properly");
            }

            return buffer;
        }
    }
}
