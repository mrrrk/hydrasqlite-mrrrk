﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using HydraSQLite.App;

namespace HydraSQLite.Services {

    public interface IEmailer {

        Task<bool> SendEmail(
            string toAddress,
            string subject,
            string body);

        Task<bool> SendEmail(
            MailAddress fromAddress,
            MailAddress toAddress,
            string subject,
            string body,
            bool isHtml);

        Task<bool> SendEmail(
            string fromAddress,
            string toAddress,
            string subject,
            string body,
            bool isHtml);
    }

    public class Emailer : IEmailer {

        private readonly IBreezeConfiguration myConfig;

        public Emailer(IBreezeConfiguration config) {
            myConfig = config;
        }

        public Task<bool> SendEmail(
            string toAddress,
            string subject,
            string body) {

            return SendEmail(new MailAddress(myConfig.AutoEmailFrom), new MailAddress(toAddress), subject, body, false);
        }

        public Task<bool> SendEmail(
            string fromAddress,
            string toAddress,
            string subject,
            string body,
            bool isHtml) {

            return SendEmail(new MailAddress(fromAddress), new MailAddress(toAddress), subject, body, isHtml);
        }

        public async Task<bool> SendEmail(
            MailAddress fromAddress,
            MailAddress toAddress,
            string subject,
            string body,
            bool isHtml) {

            try {
                var msg = new MailMessage();
                msg.Headers.Add("MimeOLE", "Produced By Microsoft Exchange V6.0.6249.0"); // do we still need this?
                msg.From = fromAddress;
                msg.To.Add(toAddress);
                msg.Subject = subject;
                msg.IsBodyHtml = isHtml;
                msg.Body = body;
                using (var smtp = new SmtpClient(myConfig.SmtpServer)) {
                    await smtp.SendMailAsync(msg);
                }               
                return true;
            }
            catch (Exception ex) {
                if (!Utils.IsDebug()) {
                    new UnhandledExceptionFilter(myConfig).SendReport(ex);
                }
                return false;
            }
        }
    }
}