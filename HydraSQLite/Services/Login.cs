﻿using System;
using System.Linq;
using System.Threading.Tasks;
using HydraSQLite.Model.Api;
using HydraSQLite.App;
using HydraSQLite.Model;
using HydraSQLite.Services.Mockable;

// Stop ReSharper moaning about dependency injection stuff:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable ClassNeverInstantiated.Global

namespace HydraSQLite.Services {

    public interface ILogin {
        User CurrentUser { get; }

        string CreateServerNOnce(string key);

        LoginResponse CheckDigest(IUrlHelperMvc urlHelper, IFormsAuthentication formsAuthentication, LoginRequest request);

        LoginResponse CheckEmailAndPassword(IUrlHelperMvc urlHelper, IFormsAuthentication formsAuthentication, LoginRequest request);

        Task<ForgotResponse> Forgot(System.Web.Http.Routing.UrlHelper urlHelper, ForgotRequest request);

        void ResetPasswordInit(ResetPassword resetPassword);

        void ResetPassword(ResetPassword resetPassword, BusinessLogic.IPasswordLogic logic);
    }

    public class Login : ILogin {

        private readonly IBreezeConfiguration myConfig;
        private readonly IEmailer myEmailer;
        private readonly IUserRepository myUserRepo;
        private readonly ICache myCache;
        private readonly IEmailUrlKey myEmailUrlKey;
        private readonly ICurrentUserIdentity myCurrentUserIdentity;

        public Login(IBreezeConfiguration config, IEmailer emailer, ICache cache, IEmailUrlKey userUrlKey, IUserRepository userRepo,  ICurrentUserIdentity currentUser) {
            myCache = cache;
            myEmailUrlKey = userUrlKey;
            myConfig = config;
            myEmailer = emailer;
            myUserRepo = userRepo;
            myCurrentUserIdentity = currentUser;
        }

        public User CurrentUser {
            get {
                //If the user isn't yet signed in, bail out
                var userID = myCurrentUserIdentity.UserID;
                System.Diagnostics.Debug.WriteLine("LoggedInUser - user ID = " + userID);
                if (userID == Guid.Empty) return null;

                var key = $"CurrentUser{userID}";
                var user = myCache.Get(key) as User;
                // user id missing or doesn't match? (should not happen)...
                if (user != null && user.ID != userID) return null;
                // hooray!
                if (user != null) return user;

                // load + store in cache
                user = myUserRepo.Load(userID);
                var expires = DateTime.Now + myConfig.ShortCachePeriod;
                myCache.Set(key, user, expires);

                return user;
            }
        }

        public string CreateServerNOnce(string key) {
            if (string.IsNullOrWhiteSpace(key)) return null;
            var nonce = Crypto.RandomString(64);
            // it's not a password reminder but that cache period will do (change if need be)
            var expires = DateTime.Now + myConfig.PasswordReminderCachePeriod; 
            myCache.Set(key, nonce, expires);
            return nonce;
        }

        public LoginResponse CheckDigest(IUrlHelperMvc urlHelper, IFormsAuthentication formsAuthentication, LoginRequest request) {

            var response = new LoginResponse();
            var results = myUserRepo.Search(new UserSearchParameters { Email = request.Email }).ToList();
            var user = results.Count == 1 ? results.First() : new User();
            if (!(myCache.Get(request.Email) is string serverNOnce)) {
                response.Message = "Login failed.  Please try again.";
                return response;
            }
            if (!results.Any())
                response.Message = "Did not recognise email or password";
            else if (!PasswordStorageDigest.VerifyPassword(
                    request.Digest, 
                    serverNOnce, 
                    request.ClientNOnce,
                    user.Password)) {
                response.Message = "Did not recognise email or password";
            }
            else if (user.Status != UserStatus.Active) {
                response.Message = "Not an active user.";
            }
            else {
                response.Message = "ok";

                // the actual ASP.NET login...
                formsAuthentication.SetAuthCookie(user.ID.ToString(), false);

                // save last logged in
                user.LastLoggedIn = DateTime.Now;
				user.PrevLastLoggedIn = user.LastLoggedIn;
                myUserRepo.Save(user);
                
                if (urlHelper.IsLocalUrl(request.RedirectUrl)
                    && request.RedirectUrl.Length > 1
                    && request.RedirectUrl.StartsWith("/")
                    && !request.RedirectUrl.StartsWith("//")
                    && !request.RedirectUrl.StartsWith("/\\")) {
                    response.RedirectUrl = request.RedirectUrl;
                }
                else {
                    response.RedirectUrl = null; //System.Web.VirtualPathUtility.ToAbsolute("~/Home/Index");
                }

                // send user data back (but not with password)
                user.Password = "";
                response.User = user;
            }
            return response;
        }

        public LoginResponse CheckEmailAndPassword(IUrlHelperMvc urlHelper, IFormsAuthentication formsAuthentication, LoginRequest request) {
            var results = myUserRepo.Search(new UserSearchParameters() { Email = request.Email }).ToList();
            
            var response = new LoginResponse();

            var user = results.Count == 1 ? results.First() : new User();

            if (string.IsNullOrWhiteSpace(request.Email) || string.IsNullOrWhiteSpace(request.Password)) {
                response.Message = "Enter a email and password";
            }
            else if (!results.Any()) {
                response.Message = "Did not recognise email or password";
            }
            else if (!PasswordStorageSecure.VerifyPassword(request.Password, user.Password)) {
                response.Message = "Did not recognise email or password";
            }
            else if (user.Status != UserStatus.Active) {
                response.Message = "Not an active user.";
            }
            else {
                response.Message = "ok";

                // the actual ASP.NET login...
                formsAuthentication.SetAuthCookie(user.ID.ToString(), false);

                // save last logged in
                user.PrevLastLoggedIn = user.LastLoggedIn;
                user.LastLoggedIn = DateTime.Now;
                myUserRepo.Save(user);

                if (urlHelper.IsLocalUrl(request.RedirectUrl)
                    && request.RedirectUrl.Length > 1
                    && request.RedirectUrl.StartsWith("/")
                    && !request.RedirectUrl.StartsWith("//")
                    && !request.RedirectUrl.StartsWith("/\\")) {
                    response.RedirectUrl = request.RedirectUrl;
                }
                else {
                    response.RedirectUrl = null;
                }

                // send user data back (but not with password hash)
                user.Password = "";
                response.User = user;
            }

            return response;
        }

        public async Task<ForgotResponse> Forgot(System.Web.Http.Routing.UrlHelper urlHelper, ForgotRequest request) {
            var response = new ForgotResponse();
            try {
                var results = myUserRepo.Search(new UserSearchParameters() { Email = request.Email }).ToList();

                if (string.IsNullOrWhiteSpace(request.Email))
                    response.Message = "The email address is blank.";
                else if (!results.Any())
                    response.Message = "Sorry, did not recognise your email.";
                else if (results.Count > 1)
                    response.Message = "Looks like there are duplicate emails!  Please contact an administrator.";
                else if (results[0].Status != UserStatus.Active)
                    response.Message = "Sorry, not an active user account.";
                else {
                    var expires = DateTime.Now + myConfig.PasswordReminderCachePeriod;
                    var key = myEmailUrlKey.Create(request.Email, expires);
                    var url = urlHelper.Link("Default", new {
                        controller="SignedOut",
                        action = "ResetPassword",
                        id = key
                    });

                   await myEmailer.SendEmail(
                        request.Email,
                        "Reset your password",
                        $"Reset your password here:\n{url}\n\nThe link will expire in {myConfig.PasswordReminderCachePeriod.Hours} hours.");
                    response.Message = "A message has been sent with a link to reset your password.  Please check your email.";
                }
            }
            catch (Exception ex) {
                response.Message = "Sorry, password reminder failed.";
                if (!Utils.IsDebug()) {
                    new UnhandledExceptionFilter(myConfig).SendReport(ex);
                }
            }
            return response;
        }

        public void ResetPasswordInit(ResetPassword resetPassword) {
            var email = myEmailUrlKey.GetEmail(resetPassword.Key);
            resetPassword.State = Model.Api.ResetPassword.StateType.None;
            if (string.IsNullOrEmpty(email)) {
                resetPassword.State = Model.Api.ResetPassword.StateType.Expired;
                resetPassword.Message = "Sorry, looks like the link has expired.  Please request another one.";
                return;
            }

            var results = myUserRepo.Search(new UserSearchParameters() { Email = email }).ToList();

            if (!results.Any())
                resetPassword.Message = "Error - email not recognised.";
            else if (results.Count > 1)
                resetPassword.Message = "Error - duplicate emails.";
            else {
                var user = results.First();
                resetPassword.State = Model.Api.ResetPassword.StateType.Ready;
                resetPassword.Email = user.Email;
            }
        }

        public void ResetPassword(ResetPassword resetPassword, BusinessLogic.IPasswordLogic logic) {
            var results = myUserRepo.Search(new UserSearchParameters() { Email = resetPassword.Email }).ToList();
            if (!results.Any()) {
                resetPassword.ValidationMessage = "Error - email not recognised.";
                return;
            }
            if (results.Count > 1) {
                resetPassword.ValidationMessage = "Error - duplicate emails.";
                return;
            }
            if (string.IsNullOrEmpty(resetPassword.Password)) {
                resetPassword.ValidationMessage = "Error - missing password.";
                return;
            }
            var check = logic.Check(resetPassword.Password);
            if (!check.IsOkay) {
                resetPassword.ValidationMessage = check.Message;
                return;
            }
            var user = results.First();
                user.Password =
                    myConfig.IsDigestAuthenticationScheme ?
                        PasswordStorageDigest.Create(resetPassword.Password) :
                        PasswordStorageSecure.Create(resetPassword.Password);
                myUserRepo.Save(user);
                resetPassword.Message = "Thank you.  Your password has been changed.";
                resetPassword.State = Model.Api.ResetPassword.StateType.Success;
                myEmailUrlKey.Remove(resetPassword.Key);
        }

    }
}