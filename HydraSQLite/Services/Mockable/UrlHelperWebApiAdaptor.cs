﻿using System.Net.Http;
using System.Web.Http.Routing;

namespace HydraSQLite.Services.Mockable {

    // This takes the 'real' UrlHelper (Web API flavour) and wraps it so it has an interface that can be mocked in tests
    // (What a pain in the arse)
    public class UrlHelperWebApiAdaptor : UrlHelper, IUrlHelperWebApi {

        internal UrlHelperWebApiAdaptor(HttpRequestMessage requestContext) : base(requestContext) { }

        public UrlHelperWebApiAdaptor(UrlHelper helper) : base(helper.Request) { }
    }
}