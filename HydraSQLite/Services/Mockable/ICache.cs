﻿using System;

namespace HydraSQLite.Services.Mockable {

    /// <summary>
    /// System.Runtime.Caching.MemoryCache doesn't implement and interface
    /// so were wrapping it an providing one here so we can inject it.
    /// </summary>
    public interface ICache {
        
        bool Contains(string key);

        object Get(string key);

        void Set(string key, object value, DateTimeOffset absoluteExpiration);

        void Remove(string key);

    }
}
