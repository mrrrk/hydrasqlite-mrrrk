﻿using System.Web.Mvc;
using System.Web.Routing;

namespace HydraSQLite.Services.Mockable {

    public class UrlHelperMvcAdaptor : UrlHelper, IUrlHelperMvc {

        internal UrlHelperMvcAdaptor(RequestContext requestContext)
            : base(requestContext) {
        }

        internal UrlHelperMvcAdaptor(RequestContext requestContext, RouteCollection routeCollection)
            : base(requestContext, routeCollection) {
        }

        public UrlHelperMvcAdaptor(UrlHelper helper)
            : base(helper.RequestContext, helper.RouteCollection) {
        }
    }
}