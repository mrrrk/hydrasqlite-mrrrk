﻿using System.Web.Routing;

namespace HydraSQLite.Services.Mockable {
    public interface IUrlHelperMvc {
        string Action(string actionName, string controllerName);

        string Action(string actionName, string controllerName, object routeValues);

        string Action(string actionName, string controllerName, RouteValueDictionary routeValues);

        bool IsLocalUrl(string url);
    }

}
