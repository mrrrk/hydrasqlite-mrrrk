﻿using System.Web.Security;

namespace HydraSQLite.Services.Mockable {

    /// <summary>
    /// Just a wrapper for FormsAuthentication so we can use an interface and 
    /// then mock it for automated testing.
    /// </summary>
    public class FormsAuthenticationWrapper : IFormsAuthentication {

        public void SetAuthCookie(string userName, bool createPersistentCookie) {
            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }
    }
}