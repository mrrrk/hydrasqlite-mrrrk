﻿using System.Collections.Generic;

namespace HydraSQLite.Services.Mockable {

    public interface IUrlHelperWebApi {

        string Link(string routeName, object routeValues);

        string Link(string routeName, IDictionary<string, object> routeValues);
    }
}
