﻿using System.Data.SqlClient;
using System.Linq;

namespace HydraSQLite.Services.Data {

    public static class SqlErrors {

        // From https://msdn.microsoft.com/en-us/library/cc645611.aspx
        // also: select * from sys.messages where language_id=1033 and severity between 11 and 16

        private enum Errors {
            None = 0,
            ForeignKeyConstraint = 547,
            RaiseError = 50000
            // ...there are 12K+ messages - so not putting them all here!
        }

        public static bool IsForeignKeyConstraintError(SqlException ex) {
            return ex != null && (from object err in ex.Errors select err as SqlError).Any(err =>
                       err.Number == (int)Errors.ForeignKeyConstraint ||
                       err.Number == (int)Errors.RaiseError
                   );
        }
    }
}