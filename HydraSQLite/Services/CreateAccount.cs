﻿using System;
using System.Linq;
using System.Threading.Tasks;
using HydraSQLite.App;
using HydraSQLite.Model;
using HydraSQLite.Model.Api;
using HydraSQLite.Services.BusinessLogic;
using HydraSQLite.Services.Mockable;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Services {

    public enum VerifyEmailResult {
        None,
        KeyLookupFailed,
        EmailLookupFailed,
        UnexpectedUserStatus,
        Success
    }

    public interface ICreateAccount {

        UserStatus GetStatus(string email);
        Task<CreateAccountResponse> StartCreateAccount(IUrlHelperWebApi urlHelper, CreateAccountRequest request);
        Task<bool> ResendVerificationEmail(IUrlHelperWebApi urlHelper, string email);
        VerifyEmailResult VerifyEmail(string key);
    }

    public class CreateAccount: ICreateAccount {

        #region " dependencies "
        
        [Unity.Attributes.Dependency]
        public IBreezeConfiguration Config { get; set; }
        
        [Unity.Attributes.Dependency]
        public IPasswordLogic PasswordLogic { get; set; }

        [Unity.Attributes.Dependency]
        public IUserLogic UserLogic { get; set; }

        [Unity.Attributes.Dependency]
        public IEmailer Emailer { get; set; }

        [Unity.Attributes.Dependency]
        public ICache Cache { get; set; }

        [Unity.Attributes.Dependency]
        public IEmailUrlKey EmailUrlKey { get; set; }

        [Unity.Attributes.Dependency]
        public IUserRepository UserRepository { get; set; }

        #endregion
        
        public UserStatus GetStatus(string email) {
            if (string.IsNullOrWhiteSpace(email)) {
                return UserStatus.None;
            }
            var user = UserRepository.Search(new UserSearchParameters { Email = email }).FirstOrDefault();
            return  user?.Status ?? UserStatus.None;
        }

        public async Task<CreateAccountResponse> StartCreateAccount(IUrlHelperWebApi urlHelper, CreateAccountRequest request) {
            var response = new CreateAccountResponse();

            var passwordCheck = PasswordLogic.Check(request.Password);
            if (!passwordCheck.IsOkay) {
                response.IsInvalidPassword = true;
                response.InvalidPasswordMessage = passwordCheck.Message;
                return response;
            }
            if (!Cache.Contains(request.CaptchaID)) {
                response.CaptchaExpired = true;
                return response;
            }
            if (!(Cache.Get(request.CaptchaID) is CaptchaImage captcha)) {
                response.CaptchaExpired = true;
                return response;
            }
            if (!string.Equals(request.CaptchaText, captcha.Text, StringComparison.CurrentCultureIgnoreCase)) {
                response.CaptchaWrong = true;
                return response;
            }

            if (string.IsNullOrWhiteSpace(request.Email)) return response;

            var user = UserRepository.Search(new UserSearchParameters { Email = request.Email }).FirstOrDefault();

            if (user?.Status == UserStatus.Active || user?.Status == UserStatus.WaitingApproval) {
                response.Status = user.Status;
                return response;
            }

            if (user == null) {
                user = new User {
                    ID = Utils.NewGuid(),
                    Email = request.Email,
                    Password = Config.IsDigestAuthenticationScheme ? PasswordStorageDigest.Create(request.Password) : PasswordStorageSecure.Create(request.Password),
                    Forename = request.Forename,
                    Surname = request.Surname,
                    Role = UserRole.Basic,
                    Status = UserStatus.WaitingEmailVerification,
                    RegistrationNotes = "",
                    Created = DateTime.Now
                };
            }
            else {
                user.Status = UserStatus.WaitingEmailVerification;
            }

            await UserLogic.PreSave(Emailer, null, user);
            UserRepository.Save(user);

            response.EmailUrlKey = await SendVerificationEmail(urlHelper, user.Email);

            response.Status = user.Status;
            return response;
        }
        
        // return false if wrong status or if email send failed
        public async Task<bool> ResendVerificationEmail(IUrlHelperWebApi urlHelper, string email) {
            var user = UserRepository.Search(new UserSearchParameters { Email = email }).FirstOrDefault();
            var key = await SendVerificationEmail(urlHelper, email);
            return user?.Status == UserStatus.WaitingEmailVerification && !string.IsNullOrEmpty(key);
        }

        public VerifyEmailResult VerifyEmail(string key) {
            var email = EmailUrlKey.GetEmail(key);
            if (string.IsNullOrWhiteSpace(email)) return VerifyEmailResult.KeyLookupFailed;
            var user = UserRepository.Search(new UserSearchParameters { Email = email }).FirstOrDefault();
            if (user == null) return VerifyEmailResult.EmailLookupFailed;
            if (user.Status != UserStatus.WaitingEmailVerification) return VerifyEmailResult.UnexpectedUserStatus;
            user.Status = UserStatus.WaitingApproval;
            UserRepository.Save(user);
            return VerifyEmailResult.Success;
        }
        
        private async Task<string> SendVerificationEmail(IUrlHelperWebApi urlHelper, string email) {
            var expires = DateTime.Now + Config.PasswordReminderCachePeriod;
            var key = EmailUrlKey.Create(email, expires);
            var url = urlHelper.Link("Default", new {
                controller = "SignedOut",
                action = "VerifyEmail",
                id = key
            });
            var body = $@"
Thank you for registering with HydraSQLite.

Please follow this link to continue the process:
{url}

After this, your details will need to be approved by one of out administrators, so plase be patient.

Please note that the above link will expire in { Config.PasswordReminderCachePeriod} hours from sending.
";
            var sentOkay = await Emailer.SendEmail(email, "Verify your email address", body);
            return sentOkay ? key : null;
        }
    }
}