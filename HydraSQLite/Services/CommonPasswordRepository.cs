﻿using System;
using System.Collections.Generic;
using System.IO;
using HydraSQLite.App;
using HydraSQLite.Services.Mockable;

namespace HydraSQLite.Services {

    public interface ICommonPasswordRepository {

        bool Contains(string password);
    }

    public class CommonPasswordRepository : ICommonPasswordRepository {

        private const string CacheKey = "common-passwords";
        private readonly ICache myCache;
        private readonly IBreezeConfiguration myConfig;

        public CommonPasswordRepository(IBreezeConfiguration config, ICache cache) {
            myCache = cache;
            myConfig = config;
        }

        private List<string> Passwords {
            get {
                try {
                    if (myCache.Get(CacheKey) is List<string> passwords) return passwords;
                    // load + store in cache
                    passwords = Load();
                    var expires = DateTime.Now + myConfig.ShortCachePeriod;
                    myCache.Set(CacheKey, passwords, expires);
                    return passwords;
                }
                catch (Exception e) {
                    new UnhandledExceptionFilter(myConfig).SendReport(e);
                    return null;
                }
            }
        }
        
        public bool Contains(string password) {
            return Passwords?.BinarySearch(password) > -1;
        }

        private List<string> Load() {
            var passwords = new List<string>();
            var filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/commonpasswords.txt");
            if (filePath == null) throw new ArgumentException("Failed to get path for passwords file");
            if (!File.Exists(filePath)) throw new FileNotFoundException("Failed to find passwords file");
            using (var filestream = new System.IO.FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                using (var file = new StreamReader(filestream, System.Text.Encoding.UTF8, true, 4096)) {
                    string lineOfText;
                    while ((lineOfText = file.ReadLine()) != null) {
                        passwords.Add(lineOfText);
                    }
                }
            }
            passwords.Sort();
            return passwords;
        }
    }
}