﻿using System;
using HydraSQLite.App;
using HydraSQLite.Services.BusinessLogic;
using HydraSQLite.Services.Data;

namespace HydraSQLite.Services {

    public interface IEmailUrlKey {
        
        string Create(string email, DateTime expires);

        string GetEmail(string key);

        void Remove(string key);
    }

    /// <summary>
    /// Database-based cache for emailed URL links to user data
    /// </summary>
    public class EmailUrlKey : IEmailUrlKey {

        private readonly IBreezeConfiguration myConfig;

        public EmailUrlKey(IBreezeConfiguration config) {
            myConfig = config;
        }

        public string Create(string email, DateTime expires) {

            // just need to call this every so often - so might as well stick it here as anywhere.
            RemoveExpired();

            var now = DateTime.Now;
            if (expires < now) {
                throw new ValidationException("Cannot be set to expire in past.");
            }
            var item = new Item() {
                Key = Crypto.RandomString(64),
                Email = email,
                Created = DateTime.Now,
                Expires = expires
            };
            new FluentData(myConfig)
                .SqlText("INSERT INTO tblEmailUrlKey (eukKey, eukEmail, eukCreated, eukExpires) VALUES (@Key, @Email, @Created, @Expires);")
                .AddParameter("@Key", item.Key)
                .AddParameter("@Email", item.Email)
                .AddParameter("@Created", item.Created.ToDatabaseValue())
                .AddParameter("@Expires", item.Expires.ToDatabaseValue())
                .ExecuteNonQuery();
            return item.Key;
        }

        public string GetEmail(string key) {
            if (key == null) return null;
            var item = new FluentData(myConfig)
                .SqlText("SELECT * FROM tblEmailUrlKey WHERE eukKey = @Key;")
                .AddParameter("@Key", key)
                .ExecuteResult(reader => new Item {
                    Key = FluentData.NonNullValue<string>(reader["eukKey"]),
                    Email = FluentData.NonNullValue<string>(reader["eukEmail"]),
                    Created = FluentData.NonNullValue<DateTime>(reader["eukCreated"]),
                    Expires = FluentData.NonNullValue<DateTime>(reader["eukExpires"])
                });
            return item == null || item.Expires < DateTime.Now ? null : item.Email;
        }

        public void Remove(string key) {
            new FluentData(myConfig)
                .SqlText("DELETE FROM tblEmailUrlKey WHERE eukKey = @Key;")
                .AddParameter("@Key", key)
                .ExecuteNonQuery();
        }

        private void RemoveExpired() {
            new FluentData(myConfig)
                .SqlText("DELETE FROM tblEmailUrlKey WHERE eukExpires < @Now;")
                .AddParameter("@Now", DateTime.Now)
                .ExecuteNonQuery();
        }

        private class Item {
            public string Key { get; set; }
            public string Email { get; set; }
            public DateTime Created { get; set; }
            public DateTime Expires { get; set; }
        }
    }
}