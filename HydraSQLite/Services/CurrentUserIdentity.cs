﻿using System;
using System.Threading;

namespace HydraSQLite.Services {

    // A way to inject the current user ID (e.g. HttpContext.Current.User...)
    public interface ICurrentUserIdentity {

        Guid UserID { get; }
    }

    public class CurrentUserIdentity : ICurrentUserIdentity {

        // Thread.CurrentPrincipal.Identity same as HttpContext.Current.User.Identity.Name
        public Guid UserID => Guid.TryParse(Thread.CurrentPrincipal.Identity?.Name ?? Guid.Empty.ToString(), out var id)
            ? id
            : Guid.Empty;
    }
}