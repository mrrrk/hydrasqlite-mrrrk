﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HydraSQLite.App;
using HydraSQLite.Model.Lookups;
using HydraSQLite.Services.Data;
using HydraSQLite.Services.Mockable;

// ReSharper disable MemberCanBePrivate.Global

namespace HydraSQLite.Services {

    /// <summary>
    /// List of lookup values + associated helper functions
    /// </summary>
    public class LookupRepository {

        private readonly ICache myCache;
        private readonly IBreezeConfiguration myConfig;

        public LookupRepository(ICache cache, IBreezeConfiguration config) {
            myConfig = config;
            myCache = cache;
        }

        private static readonly object myLock = new object();

        public LookupRepository(ICache cache, IBreezeConfiguration config, Group group) {
            myCache = cache;
            myConfig = config;
            Group = group;
            Description = Group.Descripton();
        }

        public Group Group { get; }

        public string Description { get; private set; }

        public IEnumerable<Item> ItemsWithZeroOption(string text) {
            var copyList = Items.ToList();
            copyList.Insert(0, new Item() { ID = 0, Description = text });
            return copyList;
        }

        public IEnumerable<Item> Items {
            get {
                var cacheKey = "lookup-" + Group;
                // check cache
                if (myCache.Get(cacheKey) is List<Item> items) return items;
                lock (myLock) {
                    // might have become available since obtaining lock
                    items = myCache.Get(cacheKey) as List<Item>;
                    if (items == null) {
                        items = Load();
                        myCache.Set(cacheKey, items, DateTime.Now + myConfig.ShortCachePeriod);
                    }
                }
                return items;
            }
        }

        public Item GetItem(int id) {
            return Items.FirstOrDefault(itm => itm.ID == id);
        }

        public Item GetItem(string description) {
            if (string.IsNullOrWhiteSpace(description))
                return null;
            description = description.Trim();
            return Items.FirstOrDefault(itm => itm.Description.Equals(description, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool Contains(int id) {
            return GetItem(id) != null;
        }

        public string ItemDescription(int id) {
            foreach (var itm in Items)
                if (itm.ID == id)
                    return itm.Description;
            return "- - -";
        }

        /// <summary>
        /// For use with kendo combo box = get ID or insert new value
        /// </summary>
        public int GetIDForExistingOrNew(string userText) {
            if (string.IsNullOrWhiteSpace(userText) || userText == "0")
                return 0;
            var id = userText.ToInt();
            if (id > 0 && Contains(id))
                // found ID - nice and simple
                return id;
            else if (id > 0)
                // number but not found - not allowed (?) 
                return 0;
            else {
                //does text exist in this lookup?
                var itm = GetItem(userText);
                if (itm == null || !itm.IsArchived) {
                    // not found - need to add new item!
                    var newItem = new Item() { Description = userText };
                    Save(newItem);
                    return newItem.ID;
                }
                else
                    // found by text - return mtching ID
                    return itm.ID;
            }
        }

        private List<Item> Load() {
            return new FluentData(myConfig)
                .SqlText("SELECT * FROM tblLookup WHERE lupGroupID = @GroupID OR @GroupID = 0 ORDER BY lupDescription;")
                .AddParameter("@GroupID", (int)Group)
                .ExecuteReader(Load);
        }

        private Item Load(IDataReader reader) {
            return new Item {
                ID = FluentData.NonNullValue<int>(reader["lupID"]),
                Description = FluentData.NonNullValue<string>(reader["lupDescription"]),
                IsArchived = FluentData.NonNullValue<bool>(reader["lupIsArchived"])
            };
        }

        public void Save(Item item) {
            // this stuff looks a mess with SQLite
            if (item.ID == 0) {
                const string insertSql = @"
INSERT INTO tblLookup (
	lupGroupID,
    lupDescription,
	lupDisplayOrder,
	lupIsArchived
)
VALUES (
	@GroupID,
    @Description,
	@DisplayOrder,
	@IsArchived
);
SELECT last_insert_rowid() AS newID;
";
                var newID = new FluentData(myConfig)
                    .SqlText(insertSql)
                    .AddParameter("@GroupID", (int) Group)
                    .AddParameter("@Description", item.Description.Trim())
                    .AddParameter("@DisplayOrder", item.DisplayOrder)
                    .AddParameter("@IsArchived", item.IsArchived)
                    .ExecuteReader(reader => FluentData.NonNullValue<int>(reader["newID"]))
                    .FirstOrDefault();
                if (newID != 0) item.ID = newID;
            }
            else {
                const string updateSql = @"
    UPDATE tblLookup SET
        lupDescription = @Description,
		lupDisplayOrder = @DisplayOrder,
		lupIsArchived = @IsArchived
    WHERE lupID = @ID and lupGroupID = @GroupID
";
                new FluentData(myConfig)
                    .SqlText(updateSql)
                    .AddParameter("@ID", item.ID)
                    .AddParameter("@GroupID", (int) Group)
                    .AddParameter("@Description", item.Description.Trim())
                    .AddParameter("@DisplayOrder", item.DisplayOrder)
                    .AddParameter("@IsArchived", item.IsArchived)
                    .ExecuteNonQuery();
            }

            // force refresh
            var cacheKey = "lookup-" + Group;
            myCache.Remove(cacheKey);
        }

        // false means archived instead of deleted
        public bool Delete(Item item) {
            bool wasDeleted;
            try {
                new FluentData(myConfig)
                    .SqlText("DELETE FROM tblLookup WHERE lupID = @ID AND lupGroupID = @GroupID;")
                    .AddParameter("@ID", item.ID)
                    .AddParameter("@GroupID", (int)Group)
                    .ExecuteNonQuery();
                wasDeleted = true;
            }
            catch (SqlException) {
                // item in use - hide instead
                // (note - should probably really run a query to check dependencies here)
                item.IsArchived = true;
                Save(item);
                wasDeleted = false;
            }
            // force refresh
            var cacheKey = "lookup-" + Group;
            myCache.Remove(cacheKey);

            return wasDeleted;
        }
    }
}