﻿using System;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Services.BusinessLogic {

    public interface ICompanyLogic {

        void PreSave(Model.Company company);
    }

    public class CompanyLogic: ICompanyLogic {

        #region " dependencies "

        [Unity.Attributes.Dependency]
        public ILogin Login { get; set; }

        #endregion

        public void PreSave(Model.Company company) {
            var user = Login.CurrentUser;
            if (user == null) throw new ValidationException("You aren't logged in!");

            company.LastUpdated = DateTime.Now;
            company.LastUpdatedByUserID = user.ID;
            if (company.CreatedByUserID == Guid.Empty) {
                company.Created = DateTime.Now;
                company.CreatedByUserID = user.ID;
            }
        }
    }
}