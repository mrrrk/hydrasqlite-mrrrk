﻿using System.Text.RegularExpressions;
using Newtonsoft.Json;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Services.BusinessLogic {

    public interface IPasswordLogic {
        PasswordLogic.CheckResult Check(string password);
    }

    public class PasswordLogic: IPasswordLogic {

        #region " dependencies "

        [Unity.Attributes.Dependency]
        public ICommonPasswordRepository CommonPasswordRepository { get; set; }

        #endregion

        // finds repeating sequences of characters
        private static readonly Regex myFindRepeatsOfUpToThreeRegex = new Regex(@"(.{3}).*\1");
        private static readonly Regex myFindRepeatsOfUpToFiveRegex = new Regex(@"(.{5}).*\1");

        // based on new NIST guidelines
        // https://nakedsecurity.sophos.com/2016/08/18/nists-new-password-rules-what-you-need-to-know/
        // password lists
        // https://github.com/danielmiessler/SecLists/tree/master/Passwords/Common-Credentials

        public CheckResult Check(string password) {
            var len = password?.Length ?? 0;
            if (len == 0) return new CheckResult(false, "Password is empty.");
            if (len < 8) return new CheckResult(false, "Password should be at least eight characters.");
            if (len > 64) return new CheckResult(false, "Password is too long.");
            if (CommonPasswordRepository.Contains(password)) return new CheckResult(false, "Password is a common password.");
            if (len < 10 && myFindRepeatsOfUpToThreeRegex.Matches(password ?? "").Count > 0) return new CheckResult(false, "Password contains repeating sequences.");
            if (myFindRepeatsOfUpToFiveRegex.Matches(password ?? "").Count > 0) return new CheckResult(false, "Password contains repeating sequences.");
            return new CheckResult(true, "");
        }

        public class CheckResult {

            public CheckResult(bool isOkay, string message) {
                IsOkay = isOkay;
                Message = message;
            }

            [JsonProperty("isOkay")]
            public bool IsOkay { get; }

            [JsonProperty("message")]
            public string Message { get; }
        }
    }
}