﻿using System;
using System.Threading.Tasks;
using HydraSQLite.App;
using HydraSQLite.Model;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Services.BusinessLogic {

    public interface IUserLogic {

        Task PreSave(IEmailer emailer, User existing, User user);
    }

    public class UserLogic: IUserLogic {

        #region " dependencies "

        [Unity.Attributes.Dependency]
        public ILogin Login { get; set; }

        #endregion

        public async Task PreSave(IEmailer emailer, User existing, User user) {
            var currentUser = Login.CurrentUser;

            // if no existing user avoid nulls 
            if (existing == null) { existing = user; }

            // basic validation (belt and braces)
            if (string.IsNullOrWhiteSpace(user.Email)) throw new ValidationException("Missing email address");
            if (!user.Email.IsValidEmail()) throw new ValidationException("Invalid email address");

            // are we doing stuff that requires privileges?
            //  - javascript UI code should prevent this - but just in case
            if (existing.Role != user.Role && (currentUser == null || currentUser.Role != UserRole.Admin || currentUser.ID == user.ID)) {
                throw new ValidationException("Not enough privileges to change user role!");
            }
            if (existing.Status != user.Status && (currentUser == null || currentUser.Role != UserRole.Admin || currentUser.ID == user.ID)) {
                throw new ValidationException("Not enough privileges to change user status!");
            }
            
            if (existing.Status != UserStatus.Deleted && user.Status == UserStatus.Deleted) {
                existing.Deleted = DateTime.Now;
            }

            // reject email
            if ((existing.Status == UserStatus.WaitingApproval || existing.Status == UserStatus.WaitingEmailVerification) &&
                user.Status == UserStatus.Deleted && 
                user.Email.IsValidEmail()) {
                // TODO - make this configurable in some way?
                var body = $@"
Sorry but we could not create an account on HydraSQLite for the following reason:

{user.RegistrationNotes}
";
                await emailer.SendEmail(user.Email, "Sorry, your account was not created.", body);
            }

            // set updated, created, deleted dates + user IDs
            //   - note logged in user will be null if not yet logged in, so ignore in that case...
            if (currentUser != null) {
                user.LastUpdated = DateTime.Now;
                user.LastUpdatedByUserID = currentUser.ID;
                if (user.CreatedByUserID == Guid.Empty) {
                    user.Created = user.LastUpdated;
                    user.CreatedByUserID = user.LastUpdatedByUserID;
                }
            }            
        }
    }
}