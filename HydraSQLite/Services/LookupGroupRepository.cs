﻿using HydraSQLite.App;
using HydraSQLite.Model.Lookups;
using HydraSQLite.Services.Mockable;

namespace HydraSQLite.Services {

    public interface ILookupGroupRepository {
        
        LookupRepository GetLookup(Group key);

        LookupRepository All { get; }

        LookupRepository LookupOne { get; }

        LookupRepository LookupTwo { get; }
    }
    
    public class LookupGroupRepository : ILookupGroupRepository {

        public LookupGroupRepository(IBreezeConfiguration config, ICache cache) {
            All = new LookupRepository(cache, config, Group.All);
            LookupOne = new LookupRepository(cache, config, Group.LookupOne);
            LookupTwo = new LookupRepository(cache, config, Group.LookupTwo);
        }
        
        public LookupRepository GetLookup(Group key) {
            switch (key) {
                case Group.All: return All;
                case Group.LookupOne: return LookupOne;
                case Group.LookupTwo: return LookupTwo;
                default: return null;
            }
        }

        public LookupRepository All { get; }

        public LookupRepository LookupOne { get; }

        public LookupRepository LookupTwo { get; }
        
    }
}