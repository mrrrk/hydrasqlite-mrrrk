﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using HydraSQLite.Model;
using HydraSQLite.App;
using HydraSQLite.Model.Api;
using HydraSQLite.Services.Data;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Services {

    public interface ICompanyRepository {

        (int count, IList<Company> list) Search(CompanySearchParameters parameters);

        Company Load(Guid id);

        void Save(Company company);

        void Delete(Company company);
    }

    public class CompanyRepository : ICompanyRepository {

        #region " dependencies "

        [Unity.Attributes.Dependency]
        public IBreezeConfiguration Config { get; set; }

        #endregion

        public (int count, IList<Company> list) Search(CompanySearchParameters parameters) {
            // NB - this code is different in this SQLite version - SQL Server can get count and page in one query.
            var list = new FluentData(Config)
                .SqlText(PageSql(parameters))
                .AddParameter("@searchText", parameters.SearchText.ToLower()) // SQLite is case sensitive
                .ExecuteReader(Load);
            var count = new FluentData(Config)
                .SqlText(CountSql(parameters))
                .AddParameter("@searchText", parameters.SearchText.ToLower()) // SQLite is case sensitive
                .ExecuteReader(reader => FluentData.NonNullValue<int>(reader["TotalCount"]))
                .FirstOrDefault();
            return (count, list);
        }

        public Company Load(Guid id) {
            return new FluentData(Config)
                .SqlText("SELECT * FROM tblCompany WHERE cmpID = @ID;")
                .AddParameter("@ID", id.ToString("B"))
                .ExecuteResult(Load);
        }

        public void Save(Company company) {
            const string sql = @"
INSERT OR REPLACE INTO tblCompany (
    cmpID,
    cmpName,
    cmpAddress1,
    cmpAddress2,
    cmpTown,
    cmpCounty,
    cmpPostCode,
    cmpDateEstablished,
    cmpLookupOneID,
    cmpLookupTwoID,
    cmpCreated,
    cmpCreatedByUserID,
    cmpLastUpdated,
    cmpLastUpdatedByUserID
)
VALUES (
    @ID,
    @Name,
    @Address1,
    @Address2,
    @Town,
    @County,
    @PostCode,
    @DateEstablished,
    @LookupOneID,
    @LookupTwoID,
    @Created,
    @CreatedByUserID,
    @LastUpdated,
    @LastUpdatedByUserID
);";
            new FluentData(Config)
                .SqlText(sql)
                .AddParameter("@ID", company.ID.ToString("B"))
                .AddParameter("@Name", company.Name)
                .AddParameter("@Address1", company.Address1)
                .AddParameter("@Address2", company.Address2)
                .AddParameter("@Town", company.Town)
                .AddParameter("@County", company.County)
                .AddParameter("@PostCode", company.PostCode)
                .AddParameter("@DateEstablished", company.DateEstablished.ToDatabaseValue())
                .AddParameter("@LookupOneID", company.LookupOneID.ToDatabaseID())
                .AddParameter("@LookupTwoID", company.LookupTwoID.ToDatabaseID())
                .AddParameter("@Created", company.Created.ToDatabaseValue())
                .AddParameter("@CreatedByUserID", company.CreatedByUserID.ToString("B"))
                .AddParameter("@LastUpdated", company.LastUpdated.ToDatabaseValue())
                .AddParameter("@LastUpdatedByUserID", company.LastUpdatedByUserID.ToString("B"))
                .ExecuteNonQuery();
        }

        // note that concatenation operator is || and not + in SQLite
        private string TextFilter(CompanySearchParameters parameters) => string.IsNullOrWhiteSpace(parameters.SearchText) ? "" : "AND (LOWER(cmpName) LIKE '%' || @searchText || '%' OR LOWER(cmpAddress1) LIKE '%' || @searchText || '%')";

        private string PageSql(CompanySearchParameters parameters) {
            var order = $"{parameters.SortSqlColumn} {(parameters.IsSortAscending ? "ASC" : "DESC")}";
            var offset = (parameters.PageNumber - 1) * Config.EndlessScrollPageSize;
            var pageSize = Config.EndlessScrollPageSize;
            return $@"
SELECT
    tblCompany.*
FROM 
	tblCompany
WHERE 1 = 1
    {TextFilter(parameters)}
ORDER BY {order}
LIMIT {pageSize}
OFFSET {offset}
";
        }

        // NB - this is just for SQLite version - SQL Server can get count and page in one query.
        private string CountSql(CompanySearchParameters parameters) {
            return $@"SELECT COUNT(cmpID) AS TotalCount FROM tblCompany WHERE 1 = 1 {TextFilter(parameters)}";
        }

        public void Delete(Company company) {
            new FluentData(Config)
                .SqlText("DELETE FROM tblCompany WHERE cmpID = @ID;")
                .AddParameter("@ID", company.ID.ToString("B"))
                .ExecuteNonQuery();
        }

        private Company Load(IDataReader reader) {
            return new Company() {
                ID = FluentData.NonNullValue<Guid>(reader["cmpID"]),
                Name = FluentData.NonNullValue<string>(reader["cmpName"]),
                Address1 = FluentData.NonNullValue<string>(reader["cmpAddress1"]),
                Address2 = FluentData.NonNullValue<string>(reader["cmpAddress2"]),
                Town = FluentData.NonNullValue<string>(reader["cmpTown"]),
                County = FluentData.NonNullValue<string>(reader["cmpCounty"]),
                PostCode = FluentData.NonNullValue<string>(reader["cmpPostCode"]),
                DateEstablished = FluentData.NonNullValue<DateTime>(reader["cmpDateEstablished"]),
                LookupOneID = FluentData.NonNullValue<int>(reader["cmpLookupOneID"]),
                LookupTwoID = FluentData.NonNullValue<int>(reader["cmpLookupTwoID"]),
                Created = FluentData.NonNullValue<DateTime>(reader["cmpCreated"]),
                CreatedByUserID = FluentData.NonNullValue<Guid>(reader["cmpCreatedByUserID"]),
                LastUpdated = FluentData.NonNullValue<DateTime>(reader["cmpLastUpdated"]),
                LastUpdatedByUserID = FluentData.NonNullValue<Guid>(reader["cmpLastUpdatedByUserID"])
            };
        }
    }
}