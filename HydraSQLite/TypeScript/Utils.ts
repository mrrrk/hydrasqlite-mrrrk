﻿/// <reference path="./Typings/index.d.ts" />

module App.Utils {

    // a GUID that is sortable in SQL server in date order
    export function guid(): string {
        function s4(): string {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        const dateHex = "000000000000" + new Date().getTime().toString(16);
        return `${s4()}${s4()}-${s4()}-4${s4().substr(1, 3)}-${s4()}-${dateHex.substr(dateHex.length - 12)}`;
    }

    // set all observables to default values - according to options
    export function resetObject(obj:any, options:any) {
        for (let i in obj) {
            if (obj.hasOwnProperty(i)) {
                const member = obj[i];

                if (typeof member === "object") {
                    // recursive call
                    resetObject(member, options);
                }
                else if (typeof member === "number") {
                    obj[i] = 0;
                }
                else if (member !== undefined && member.pop !== undefined && typeof member.pop === "function") {
                    let result;
                    do {
                        result = member.pop();
                    } while (result);
                } else if (typeof member === "function" && ko.isObservable(member) && !ko.isComputed(member)) {
                
                    if (options && options.numbers && options.numbers instanceof Array && options.numbers.indexOf(i) > -1) {
                        member(0);
                    }
                    else if (options && options.dates && options.dates instanceof Array && options.dates.indexOf(i) > -1) {
                        member(new Date(-62135596800000)); // 01/01/0001
                    }
                    else if (options && options.bools && options.bools instanceof Array && options.bools.indexOf(i) > -1) {
                        member(false);
                    }
                    else if (typeof (member()) === "number") {
                        member(0);
                    }
                    else if (member() instanceof Date) {
                        member(new Date(-62135596800000)); // 01/01/0001
                    }
                    else if (typeof (member()) === "boolean") {
                        member(false);
                    }
                    else {
                        member("");
                    }
                }
                else if (typeof member === "string") {
                    obj[i] = "";
                }
            }
        }
    }

    // recursive check for dirty = true
    export function isDirty(model: any): boolean {
        for (let key in model) {
            if (model.hasOwnProperty(key)) {
                const item = model[key];
                //console.log("seeIfDirty " + key);
                if (ko.isObservable(item) && typeof item.isDirty === "function" && item.isDirty()) {
                    return true;
                }
                else if (typeof item === "object" && key.substring(0, 1) !== "_") {
                    const isSubDirty = this.isDirty(item);
                    if (isSubDirty) return true;
                }
                else if (ko.isObservable(item) && item() instanceof Array) {
                    for (let i = 0; i < item().length; i++) {
                        const isSubDirty = this.isDirty(item()[i]);
                        if (isSubDirty) return true;
                    }
                }
            }
        }
        return false;
    }

    export function setNotDirty(model: any): void {
        for (let key in model) {
            if (model.hasOwnProperty(key)) {
                const item = model[key];
                if (ko.isObservable(item) && typeof item.isDirty === "function" && item.isDirty()) {
                    //console.log("not dirty: " + key + "='" + item() + "'");
                    item.isDirty(false);
                    item.originalValue = item();
                }
                else if (typeof item === "object") {
                    this.setNotDirty(item);
                }
                else if (ko.isObservable(item) && item() instanceof Array) {
                    for (let i = 0; i < item().length; i++) {
                        this.setNotDirty(item()[i]);
                    }
                }
            }
        }
    }

    // get plain JS object from KO model - without computeds, etc.
    export function copyModel(model: any): any {
        const data = ko.toJS(model);
        for (let key in model) {
            if (model.hasOwnProperty(key)) {
                const member = model[key];
                //console.log("key=" + key + "  comp=" + ko.isComputed(model[key]));
                if (ko.isComputed(member) || key.substring(0, 1) === "_") {
                    delete data[key];
                }

                //TODO - make this recursive?  but delete child objects and arrays for now
                if (ko.isObservable(member) && member() instanceof Array) {
                    delete data[key];
                }
                if (!ko.isObservable(member)) {
                    delete data[key];
                }
            }
        }
        return data;
    }

    export function getLookupDescription(items:Array<Model.LookupItem>, id:any): string {
        const match = ko.utils.arrayFirst(items, (item:Model.LookupItem) => equalIDs(id, item.id()));
        if (!match && !!id && id !== "00000000-0000-0000-0000-000000000000" && id !== "'00000000000000000000000000000000") {
            // we have an id but no match
            return `Lookup failed: ${id}`;
        }
        else {
            // is either match or there is no id set
            const text = (!match) ? "- - -" : match.description ? match.description() : "- - -";
            return text;
        }
    }

    // try to equate IDs regardless of type or casing
    function equalIDs (a:any, b:any): boolean {
        const atyp = typeof (a);
        const btyp = typeof (b);
        if (atyp === "number" && btyp === "number") return a === b;
        if (atyp === "string")  { a = a.toUpperCase(); }
        if (btyp === "string") { b = b.toUpperCase(); }
        return String(a) === String(b);
    }

    // get last part of URL
    export function getUrlID(): string {
        const url = document.URL;
        return url.substr(url.lastIndexOf("/") + 1);
    }

    // get URL query string parameter
    export function getParameterByName(name: string, url?:string) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
        const results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return "";
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    export function getFriendlyName(text: string): string {
        if (!text) return "";
        const result = text.replace(/([A-Z])/g, " $1");
        return result.charAt(0).toUpperCase() + result.slice(1); 
    }

    //
    // date formatting 
    //

    export function whenText(d: any): string {
        const dte = moment(d);
        if (!dte.isValid() || dte.year() < 1753) { return "- - -"; }
        const daysDiff = moment().startOf("day").diff(moment(d).startOf("day"), "days");
        if (daysDiff < 0) { return "in the future"; }
        if (daysDiff === 1) { return "yesterday"; }
        if (daysDiff > 7) { return moment(d).format("DD MMM YYYY"); }
        if (daysDiff > 1) { return `${daysDiff} days ago`; }
        const hoursDiff = moment().diff(dte, "hours");
        if (hoursDiff === 1) { return "an hour ago"; }
        if (hoursDiff > 1) { return `${hoursDiff} hours ago`; }
        const minsDiff = moment().diff(dte, "minutes");
        if (minsDiff === 1) { return "a minute ago"; }
        if (minsDiff > 1) { return `${minsDiff} mins ago`; }
        return "just now";
    }

    export function dateText(d: any): string {
        const m = moment(d);
        if (!m.isValid() || m.year() < 1753) {
            return "- - -";
        }
        return moment(d).format("DD MMM YYYY");
    }

    export function dateTimeText(d: any): string {
        const m = moment(d);
        if (!m.isValid() || m.year() < 1753) {
            return "- - -";
        }
        // http://momentjs.com/docs/#/displaying/
        return moment(d).format("DD MMM YYYY HH:mm");
    };

    //
    // -- checking types
    //

    export function isDate(d: any) {
        if (!d) return false;
        return !!d.getMonth;
    }

    export function hasDateValue(d: any) {
        return isDate(d) && (+(d)) > -6847804800000; // 1753 = min SQL date
    }

    export function isBlankOrZero(text: any) {
        if (!text) return true;
        const str = String(text);
        return str === "0" || isEmptyOrWhitespace(text);
    };

    export function isEmptyOrWhitespace(text: string) {
        if (!text) return true;
        // see if any non-whitespace
        return !(/\S/.test(text));
    };

    export function isValidPostcode(postcode: string) {
        postcode = postcode.replace(/\s/g, "");
        const regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
        return regex.test(postcode);
    };

    export function isValidEmail(email: string) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    export function isString(obj: any) {
        const toString = Object.prototype.toString;
        return toString.call(obj) === "[object String]";
    };

    export function isFunction(functionToCheck: any) {
        const getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === "[object Function]";
    };

    export function debounce(func, wait:number, immediate = false) {
        var timeout: number;
        return function () {
            const context = this;
            const args = arguments;
            const later = () => {
                timeout = null;
                if (!immediate) func.apply(this, args);
            };
            const callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };
}