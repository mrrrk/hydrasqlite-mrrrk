﻿// extend typing for datetimepicker
interface JQuery {
    datetimepicker(key: string, value: any): JQuery;
    datetimepicker(options: any): JQuery;
}

interface KnockoutBindingHandlers {
    yesNoText: KnockoutBindingHandler;
    twoDecPlacesText: KnockoutBindingHandler;
    twoDecPlacesValue: KnockoutBindingHandler;
    codeNumberText: KnockoutBindingHandler;
    dateText: KnockoutBindingHandler;
    dateTimeText: KnockoutBindingHandler;
    whenText: KnockoutBindingHandler;
    fileSizeText: KnockoutBindingHandler;
    datetimepicker: KnockoutBindingHandler;
    setTime: KnockoutBindingHandler;
    enterKey: KnockoutBindingHandler;
    escapeKey: KnockoutBindingHandler;
    lookupText: KnockoutBindingHandler;
    toggleText: KnockoutBindingHandler;
    truncateText: KnockoutBindingHandler;
    slideVisible: KnockoutBindingHandler;
}