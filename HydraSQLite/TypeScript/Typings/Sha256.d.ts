﻿// incomplete definitions for sha256.js

interface Sha256Static {
    hash: (text: string) => string
}

declare var Sha256: Sha256Static;