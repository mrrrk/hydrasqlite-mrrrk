﻿// for tying DOM element for message to validation logic
interface KnockoutBindingHandlers {
    validationMessage: KnockoutBindingHandler;
}

// extra fields for validation
interface KnockoutObservable<T> {
    isValid: boolean;
    validate: () => void;
    shouldShowMessage: boolean;
    //showValidation: () => void;
    //hideValidation: () => void;
    domElements: HTMLElement[];
    validationOptions: App.ValidationOptions;
}

// extenders for validation
interface KnockoutExtenders {
    domElement: (target: KnockoutObservable<any>, element: HTMLElement) => void;
    validate: (target: KnockoutObservable<any>, options: any) => KnockoutObservable<any>;
}
