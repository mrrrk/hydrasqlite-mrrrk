﻿// class for supplying options in the model class
module App {
    export class ValidationOptions {
        rule: () => boolean;
        message: string;
    }
}

// used to specify the html element that will show the validation message
// in conjunction with the below extenders
ko.bindingHandlers.validationMessage = {
    init: (element, valueAccessor) => {
        // assign reference to DOM element to the observable so it knows what element to put the message on
        valueAccessor().extend({ domElement: element });
    }
};

// used to specify the html element that will show the validation message
// in conjuction with the above custom binding
ko.extenders.domElement = (target, element) => {
    if (!target.domElements) target.domElements = [];
    target.domElements.push(element);
    return target;
}

// execute the validation and show the message
ko.extenders.validate = (target: KnockoutObservable<any>, options: App.ValidationOptions) => {
    target.validationOptions = options;
    target.isValid = true;

    // run the validation on this observable
    target.validate = () => {
        if (!target.validationOptions.rule()) {
            console.log("Validation failed: " + target.validationOptions.message);
        }
        const shouldShow = target.shouldShowMessage && !target.validationOptions.rule();
        showHideMessage(shouldShow);
    }

    function showHideMessage(shouldShow: boolean) {
        target.isValid = !shouldShow;
        if (target.domElements) {
            for (let i = 0; i < target.domElements.length; i++) {
                const jqElement = $(target.domElements[i]);
                const popover = jqElement.popover({ placement: "top", trigger: "hover focus" });
                if (shouldShow) {
                    jqElement.addClass("inner-glow");
                    jqElement.attr("data-content", target.validationOptions.message);
                    popover.popover("enable");
                } else {
                    jqElement.removeClass("inner-glow");
                    popover.popover("disable");
                }
            }
        }
    }

    // subscribe to the actual observable being validated 
    // so that it is re-validated when it changes
    target.subscribe(target.validate);

    return target;
};

//
// -- recursive helper functions
//

module App.Validation {

    // set showMessage flag on all observables in tree
    // and then run the validation which will show or hide the message
    export function shouldShowMessages(model: any, shouldShow: boolean): void {
        for (let key in model) {
            if (model.hasOwnProperty(key)) {
                const item = model[key];
                if (ko.isObservable(item) && typeof item.validationOptions !== "undefined") {
                    item.shouldShowMessage = shouldShow;
                    item.validate();
                }
                else if (typeof item === "object") {
                    this.shouldShowMessages(item, shouldShow);
                }
                else if (ko.isObservable(item) && item() instanceof Array) {
                    for (let i = 0; i < item().length; i++) {
                        this.shouldShowMessages(item()[i], shouldShow);
                    }
                }
            }
        }
    }

    // recursive check for isValid = false
    export function isValid(model: any, recursion?: App.ValidationType): boolean {
        recursion = recursion || ValidationType.Recursive;
        for (let key in model) {
            if (model.hasOwnProperty(key)) {
                const item = model[key];
                if (ko.isObservable(item) && typeof item.isValid === "boolean" && !item.isValid) {
                    console.log("Not valid: " + " (" + key + ") " + item.validationOptions.message);
                    return false;
                }
                else if (recursion === ValidationType.Recursive && typeof item === "object" && key.substring(0, 1) !== "_") {
                    const isSubValid = this.isValid(item);
                    if (!isSubValid) return false;
                }
                else if (recursion === ValidationType.Recursive && ko.isObservable(item) && item() instanceof Array) {
                    for (let i = 0; i < item().length; i++) {
                        const isSubValid = this.isValid(item()[i]);
                        if (!isSubValid) return false;
                    }
                }
            }
        }
        return true;
    }

    // run validation on all observables in tree that support it
    export function validate(model: any): void {
        for (let key in model) {
            if (model.hasOwnProperty(key)) {
                const item = model[key];
                if (ko.isObservable(item) && typeof item.validate === "function") {
                    item.validate();
                }
                else if (typeof item === "object") {
                    this.validate(item);
                }
                else if (ko.isObservable(item) && item() instanceof Array) {
                    for (let i = 0; i < item().length; i++) {
                        this.validate(item()[i]);
                    }
                }
            }
        }
    }
}