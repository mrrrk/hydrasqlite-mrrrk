﻿ko.bindingHandlers.yesNoText = {
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        element.textContent = (value) ? "Yes " : "No ";
    }
};


ko.bindingHandlers.twoDecPlacesText = {
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        element.textContent = value.twoDecPlaces();
    }
};

ko.bindingHandlers.twoDecPlacesValue = {
    init(element, valueAccessor) {
        ko.utils.registerEventHandler(element, "focusout", () => {
            const observable = valueAccessor();
            observable(element.value.twoDecPlaces());
        });
    },
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        element.value = value.twoDecPlaces();
    }
};

ko.bindingHandlers.codeNumberText = {
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        element.textContent = value.pad(5);
    }
};
ko.bindingHandlers.dateText = {
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        element.textContent = App.Utils.dateText(value);
    }
};
ko.bindingHandlers.dateTimeText = {
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        element.textContent = App.Utils.dateTimeText(value);
    }
};
ko.bindingHandlers.whenText = {
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        element.textContent = App.Utils.whenText(value);
    }
};
ko.bindingHandlers.fileSizeText = {
    update(element, valueAccessor) {
        const bytes = +(ko.utils.unwrapObservable(valueAccessor()));
        const decimals = 1; // can change this as required...
        let text = "0 bytes";
        if (bytes > 0) {
            const k = 1000; // or 1024 for binary
            const dm = decimals + 1 || 3;
            const sizes = ["bytes", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
            const i = Math.floor(Math.log(bytes) / Math.log(k));
            text = parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
        }
        element.textContent = text;
    }
};

ko.bindingHandlers.datetimepicker = {
    init(element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        const options = allBindingsAccessor().datepickerOptions || {};

        const blurFunc = () => {
            const observable = valueAccessor();
            $(element).datetimepicker("update", element.value);
            if (App.Utils.isEmptyOrWhitespace(element.value))
                observable(new Date(-62135596800000)); // 01/01/0001
            else
                observable(new Date(element.value));
        };

        $(element).datetimepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "changeDate", e => {
            const observable = valueAccessor();
            const date = new Date(e.date.getTime());
            date.setMinutes(e.date.getMinutes() + e.date.getTimezoneOffset());
            date.setHours(0, 0, 0, 0);
            observable(date);
        });

        ko.utils.registerEventHandler(element, "blur", blurFunc);

        ko.utils.registerEventHandler(element, "show", () => {
            $(element).off("blur");
        });

        ko.utils.registerEventHandler(element, "hide", () => {
            $(element).blur(blurFunc);
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, () => {
            $(element).datetimepicker("destroy");
        });
    },
    //update the control when the view model changes
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        if (App.Utils.hasDateValue(value)) {
            $(element).datetimepicker("setDate", value);
        }
        else
            $(element).val("");
    }
};

ko.bindingHandlers.setTime = { //using dropdown to select time instead of picker
    init(element, valueAccessor) {
        ko.utils.registerEventHandler(element, "blur", () => {
            const observable = valueAccessor();
            if (App.Utils.isDate(observable())) {
                const hour = Math.floor(element.selectedIndex / 2);
                const minutes = element.selectedIndex % 2 === 0 ? 0 : 30;
                const value = observable();
                value.setHours(hour, minutes);
                observable(value);
            }
        });
    },
    update(element, valueAccessor) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        if (App.Utils.isDate(value)) {
            let index = value.getHours() * 2;
            if (value.getMinutes() === 30)
                index += 1;
            element.selectedIndex = index;
        }
    }
};

// binding to set action of enter key
ko.bindingHandlers.enterKey = {
    init(element, valueAccessor, allBindingsAccessor, viewModel) {
        const allBindings = allBindingsAccessor();
        $(element).on("keypress", "input, textarea, select", (e:any) => {
            const keyCode = e.which || e.keyCode;
            if (keyCode !== 13) {
                return true;
            }
            const target = e.target as HTMLElement;

            // blur doesn't behave in Edge (it would seem) so simulate a tab press to move focus on to next item
            const inputs = $(":input, button");
            const nextInput: HTMLElement = inputs.get(inputs.index(target) + 1) as any;
            (!!nextInput) ? nextInput.focus() : target.blur();
            
            allBindings.enterKey.call(viewModel, viewModel, target, element);
            return false;
        });
    }
};

ko.bindingHandlers.escapeKey = {
    init(element, valueAccessor, allBindingsAccessor, viewModel) {
        const allBindings = allBindingsAccessor();
        $(element).on("keypress", "input, textarea, select", (e:any) => {
            const keyCode = e.which || e.keyCode;
            if (keyCode !== 27) {
                return true;
            }
            const target = e.target as HTMLElement;
            const inputs = $(":input, button");
            const nextInput: HTMLElement = inputs.get(inputs.index(target) + 1) as any;
            (!!nextInput) ? nextInput.focus() : target.blur();
            allBindings.escapeKey.call(viewModel, viewModel, target, element);
            return false;
        });
    }
};

ko.bindingHandlers.lookupText = {
    update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        const value = ko.utils.unwrapObservable(valueAccessor());
        if (value == undefined)
            element.textContent = "- - -";
        else {
            const lookupGroup = allBindingsAccessor().lookupGroup;
            let lookups = allBindingsAccessor().lookups;

            if (!!lookups && typeof lookups === "function") {
                // get underlying array from observablearray
                lookups = lookups();
            }

            if (lookups === undefined || !(lookups instanceof Array) && !!lookupGroup) {
                //if (app.viewModel !== undefined && App.viewModel.lookups !== undefined && App.viewModel.lookups[lookupGroup] !== undefined) {
                //    lookups = App.viewModel.lookups[lookupGroup]();
                //}
                //else
                if (!!bindingContext.$parent[lookupGroup]) {
                    lookups = bindingContext.$parent[lookupGroup]();
                }
                else if (viewModel !== undefined && !!viewModel[lookupGroup]) {
                    lookups = viewModel[lookupGroup]();
                }
            }

            if (!(lookups instanceof Array)) {
                throw "Unable to process lookupText binding.";
            }

            const desc = App.Utils.getLookupDescription(lookups, value);
            if (element.tagName.toUpperCase() === "INPUT")
                element.value = desc ? desc : "- - -";
            else
                element.textContent = desc ? desc : "- - -";
        }
    }
};

ko.bindingHandlers.toggleText = {
    init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        const maxLength = allBindingsAccessor().maxLength;
        $(element.nextElementSibling).on("click", function () { //anchor tag click to show more/less text if max length exceeded
            const observable = valueAccessor();
            if (this.innerHTML === "more") {
                ko.bindingHandlers.text.update(element, () => observable(), allBindingsAccessor, viewModel, bindingContext);
                this.innerHTML = "less";
            }
            else {
                ko.bindingHandlers.text.update(element, () => (observable().substring(0, maxLength).trim() + "..."), allBindingsAccessor, viewModel, bindingContext);
                this.innerHTML = "more";
            }
        });
    },
    update(element, valueAccessor, allBindingsAccessor) {
        const maxLength = allBindingsAccessor().maxLength;
        const value = ko.utils.unwrapObservable(valueAccessor());
        if (value.length > maxLength) {
            element.textContent = value.substring(0, maxLength).trim() + "...";
            $(element.nextElementSibling).show();
            $(element.nextElementSibling).text("more");
        }
        else {
            element.textContent = value;
            $(element.nextElementSibling).hide();
            $(element.nextElementSibling).text("less");
        }
    }
};

ko.bindingHandlers.truncateText = {
    update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        const maxLength = ko.utils.unwrapObservable(allBindingsAccessor().maxLength);
        const originalText = ko.utils.unwrapObservable(valueAccessor());
        const truncatedText = originalText.length > maxLength ? originalText.substring(0, maxLength) + "..." : originalText;
        ko.bindingHandlers.text.update(element, () => truncatedText, allBindingsAccessor, viewModel, bindingContext);
    }
};

ko.bindingHandlers.slideVisible = {
    update: (element, valueAccessor, allBindings) => {
        // First get the latest data that we're bound to
        var value = valueAccessor();

        // Next, whether or not the supplied model property is observable, get its current value
        var valueUnwrapped = ko.unwrap(value);

        // Grab some more data from another binding property
        var duration = allBindings.get('slideDuration') || 400; // 400ms is default duration unless otherwise specified

        // Now manipulate the DOM element
        if (!!valueUnwrapped)
            $(element).slideDown(duration); // Make the element visible
        else
            $(element).slideUp(duration);   // Make the element invisible
    }
};