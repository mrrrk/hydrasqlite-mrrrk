﻿namespace App.Model {

    export class LookupItem {

        constructor() {
            this.setUpValidation();
        }

        id: KnockoutObservable<any> = ko.observable(0);
        description: KnockoutObservable<string> = ko.observable("");
        isArchived: KnockoutObservable<boolean> = ko.observable(false);

        reset = () => {
            this.id(0);
            this.description("");
            this.isArchived(false);
        }

        private setUpValidation = () => {
            this.description.extend({
                validate: {
                    rule: () => { return !Utils.isEmptyOrWhitespace(this.description()); },
                    message: "Please enter a description"
                }
            });
        }

        static mapping = {
            key(data) { return ko.utils.unwrapObservable(data.id); },
            create(options) {
                const item = new LookupItem();
                ko.mapping.fromJS(options.data, {}, item);
                return item;
            }
        }
    }


}