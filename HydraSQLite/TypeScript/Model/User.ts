﻿namespace App.Model {

    export class User {

        constructor() {
            this.setUpValidation();
            // trigger password validation when server message returns
            this.passwordServerValidationMessage.subscribe(() => {
                this.newPassword.validate();
            });
        }

        id: KnockoutObservable<string> = ko.observable(App.emptyGuidValue);
        role: KnockoutObservable<number> = ko.observable(0);
        status: KnockoutObservable<number> = ko.observable(0);
        email: KnockoutObservable<string> = ko.observable("");
        forename: KnockoutObservable<string> = ko.observable("");
        surname: KnockoutObservable<string> = ko.observable("");
        registrationNotes: KnockoutObservable<string> = ko.observable("");
        password: KnockoutObservable<string> = ko.observable("");
        lastLoggedIn: KnockoutObservable<Date> = ko.observable(new Date(App.emptyDateValue));
        prevLastLoggedIn: KnockoutObservable<Date> = ko.observable(new Date(App.emptyDateValue));
        created: KnockoutObservable<Date> = ko.observable(new Date(App.emptyDateValue));
        createdByUserID: KnockoutObservable<string> = ko.observable(App.emptyGuidValue);
        lastUpdated: KnockoutObservable<Date> = ko.observable(new Date(App.emptyDateValue));
        lastUpdatedByUserID: KnockoutObservable<string> = ko.observable(App.emptyGuidValue);
        deleted: KnockoutObservable<Date> = ko.observable(new Date(App.emptyDateValue));
        deletedByUserID: KnockoutObservable<string> = ko.observable(App.emptyGuidValue);

        newPassword: KnockoutObservable<string> = ko.observable("");
        repeatPassword: KnockoutObservable<string> = ko.observable("");
        passwordServerValidationMessage: KnockoutObservable<string> = ko.observable("");
        passwordValidationMessage: string = "";
        
        fullName: KnockoutComputed<string> = ko.computed(() => `${this.forename()} ${this.surname()}`);

        statusRowCss: KnockoutComputed<string> = ko.computed(() => {
            if (this.status() === UserStatus.WaitingEmailVerification) return "warning-background";
            if (this.status() === UserStatus.WaitingApproval) return "success-background";
            if (this.status() === UserStatus.Deleted) return "danger-background";
            return "";
        });

        statusIconCss: KnockoutComputed<string> = ko.computed(() => {
            if (this.status() === UserStatus.WaitingEmailVerification) return "fa fa-user-circle-o warning-color";
            if (this.status() === UserStatus.WaitingApproval) return "fa fa-user-circle-o success-color";
            if (this.status() === UserStatus.Deleted) return "fa fa-ban danger-color";
            return "fa fa-user primary-color";
        });

        statusText: KnockoutComputed<string> = ko.computed(() => {
            if (this.status() === UserStatus.WaitingEmailVerification) return "Email not verified";
            if (this.status() === UserStatus.WaitingApproval) return "Awaiting approval";
            if (this.status() === UserStatus.Active) return "Active";
            if (this.status() === UserStatus.Deleted) return "Deleted";
            return "- - -";
        });
        
        reset = () => { 
            Utils.resetObject(this, {});
            this.id(App.emptyGuidValue);
            this.lastLoggedIn(new Date(App.emptyDateValue));
            this.prevLastLoggedIn(new Date(App.emptyDateValue));
            this.created(new Date(App.emptyDateValue));
            this.createdByUserID(App.emptyGuidValue);
            this.lastUpdated(new Date(App.emptyDateValue));
            this.lastUpdatedByUserID(App.emptyGuidValue);
            this.deleted(new Date(App.emptyDateValue));
            this.deletedByUserID(App.emptyGuidValue);
        }

        // called from constructor
        private setUpValidation = () => {
            this.forename.extend({
                validate: {
                    rule: () => { return !Utils.isEmptyOrWhitespace(this.forename()); },
                    message: "Please enter a forename"
                }
            });

            this.surname.extend({
                validate: {
                    rule: () => { return !Utils.isEmptyOrWhitespace(this.surname()); },
                    message: "Please enter a surname"
                }
            });

            this.email.extend({
                validate: {
                    rule: () => {
                        const validationOptions = this.email.validationOptions as ValidationOptions;
                        if (Utils.isEmptyOrWhitespace(this.email())) {
                            validationOptions.message = "Please supply an email address";
                            return false;
                        }
                        if (!Utils.isValidEmail(this.email())) {
                            validationOptions.message = "Email address is not valid";
                            return false;
                        }
                        return true;
                    }
                }
            });

            this.newPassword.extend({
                validate: {
                    rule: () => {
                        const isNewAndBlank = (Utils.isBlankOrZero(this.id()) && Utils.isEmptyOrWhitespace(this.newPassword()));
                        const isNotBlankButTooShort = (!Utils.isEmptyOrWhitespace(this.newPassword()) && this.newPassword().length < 8);
                        const isServerMessage = !Utils.isEmptyOrWhitespace(this.passwordServerValidationMessage());

                        this.passwordValidationMessage =
                            isNewAndBlank ? "Please enter a password" :
                            isNotBlankButTooShort ? "Password should be at least eight characters" :
                            isServerMessage ? this.passwordServerValidationMessage() : "";

                        console.log("*** user validation: msg=" + this.passwordValidationMessage + " isSvr=" + isServerMessage + " smsg=" + this.passwordServerValidationMessage());

                        return !(isNewAndBlank || isNotBlankButTooShort || isServerMessage);
                    },
                    message: () => {
                        return this.passwordValidationMessage;
                    }
                }
            });

            this.repeatPassword.extend({
                validate: {
                    rule: () => {
                        const isSomething = !Utils.isEmptyOrWhitespace(this.newPassword());
                        const isMatch = this.newPassword() !== this.repeatPassword();
                        const isOk = !(isSomething && isMatch);
                        return isOk;
                    },
                    message: "Passwords must match"
                }
            });
        }

        static mapping = {
            key(data) { return ko.utils.unwrapObservable(data.id); },
            create(options) {
                const user = new User();
                ko.mapping.fromJS(options.data, {}, user);
                return user;
            }
        }
    }
}
