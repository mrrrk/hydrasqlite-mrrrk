﻿namespace App.Model {

    export class CompanySearchParameters {

        constructor() {
            this.reset();
        }

        lastPageLoaded = 0;
        pageNumber = 1;

        isSortAscending: KnockoutObservable<boolean> = ko.observable(true);
        // TODO - make this an enum?
        sortField: KnockoutObservable<string> = ko.observable("Name");
        searchText: KnockoutObservable<string> = ko.observable("");

        reset = () => {
            this.lastPageLoaded = 0;
            this.pageNumber = 1;
            this.searchText("");
            this.sortField("Name");
            this.isSortAscending(true);
        }
    }
}