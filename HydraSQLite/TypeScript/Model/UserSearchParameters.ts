﻿namespace App.Model {

    export class UserSearchParameters {

        constructor() {
            this.reset();
        }

        statuses: KnockoutObservableArray<number> = ko.observableArray([UserStatus.Active, UserStatus.WaitingApproval]);
        text: KnockoutObservable<string> = ko.observable("");
        
        reset = () => {
            this.text("");
            this.statuses([UserStatus.Active, UserStatus.WaitingApproval]);
        }
    }
}