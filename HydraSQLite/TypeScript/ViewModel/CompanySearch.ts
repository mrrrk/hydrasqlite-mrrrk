﻿namespace App.ViewModel {

    export class CompanySearch {

        //
        // -- properties
        //

        // lookups
        lookupOnes: KnockoutObservableArray<Model.LookupItem> = ko.observableArray([]);
        lookupTwos: KnockoutObservableArray<Model.LookupItem> = ko.observableArray([]);

        // other vm properties
        parameters = new Model.CompanySearchParameters();
        totalCount = ko.observable(0);
        companies: KnockoutObservableArray<Model.Company> = ko.observableArray([]);
        company = new Model.Company();

        // computed observables
        sortStateName: KnockoutComputed<string> =
            ko.computed({ owner: this, read: () => { return this.getSortStateCssClasses("Name"); } });
        sortStateTown: KnockoutComputed<string> =
            ko.computed({ owner: this, read: () => { return this.getSortStateCssClasses("Town"); } });
        sortStateCounty: KnockoutComputed<string> =
            ko.computed({ owner: this, read: () => { return this.getSortStateCssClasses("County"); } });
        sortStatePostCode: KnockoutComputed<string> = ko.computed({
            owner: this,
            read: () => { return this.getSortStateCssClasses("PostCode"); }
        });
        sortStateLookupOne: KnockoutComputed<string> = ko.computed({
            owner: this,
            read: () => { return this.getSortStateCssClasses("LookupOne"); }
        });
        sortStateLookupTwo: KnockoutComputed<string> = ko.computed({
            owner: this,
            read: () => { return this.getSortStateCssClasses("LookupTwo"); }
        });
        sortStateCreated: KnockoutComputed<string> = ko.computed({
            owner: this,
            read: () => { return this.getSortStateCssClasses("Created"); }
        });
        sortStateLastUpdated: KnockoutComputed<string> = ko.computed({
            owner: this,
            read: () => { return this.getSortStateCssClasses("LastUpdated"); }
        });

        companyTitle: KnockoutComputed<string> = ko.computed(() => Utils.isEmptyOrWhitespace(this.company.id())
            ? "New Company"
            : `Edit ${this.company.name()}`);

        //
        // -- methods
        //

        // called after DOM has loaded
        onLoad = () => {
            this.loadLookups();
        }

        private getSortStateCssClasses(field: string) {
            const isSelected = field === this.parameters.sortField();
            return `fa fa-arrow-${isSelected ? "circle-" : ""}${isSelected && !this.parameters.isSortAscending() ? "down" : "up"}`;
        }

        search = () => {
            
            this.companies([]); // since paging, need to clear results for fresh search
            this.parameters.pageNumber = 1;
            this.doSearch();
        }

        loadNextPage = () => {
            if (this.companies().length === 0) {
                console.log("*** loadNextPage - no results - cannot get next");
                return;
            }
            if (this.companies().length >= this.totalCount()) {
                console.log("*** loadNextPage - got all results already");
                return;
            }
            if (this.parameters.lastPageLoaded < this.parameters.pageNumber) {
                console.log("*** loadNextPage - skip because missing page");
                return;
            }
            console.log("*** loadNextPage");
            this.parameters.pageNumber++;
            this.doSearch();
        }

        sort = (data: any, event: Event) => {
            const sortField = (event.currentTarget as HTMLElement).getAttribute("data-sortfield");

            if (sortField === this.parameters.sortField()) {
                // reverse...
                this.parameters.isSortAscending(!this.parameters.isSortAscending());
            }
            else {
                this.parameters.sortField(sortField);
                this.parameters.isSortAscending(true);
            }
            this.search();
        }

        editNew = () => {
            // need to update currently bound object - replacing it with new instance screws up bindings
            this.company.reset();
            Validation.shouldShowMessages(this.company, false);
            $("#editCompanyModal").modal("show");
        }

        edit = (company: Model.Company) => {
            JsonWebApi.get(`Api/CompanyApi/Load?id=${company.id()}`, (responseData) => {
                ko.mapping.fromJS(responseData, Model.Company.mapping, this.company);
                    Validation.shouldShowMessages(this.company, false);
                    $("#editCompanyModal").modal("show");
            });
        }

        cancelEdit = () => {
            $("#editCompanyModal").modal("hide");
            this.company.reset();
        }

        save = (data: Model.Company, event: Event) => {
            Validation.shouldShowMessages(this.company, true);
            if (!Validation.isValid(this.company)) {
                toastr.warning("Please fix the highlighted problems.", "Cannot Save");
                return;
            }
            //assign an ID if it's new
            if (Utils.isEmptyOrWhitespace(this.company.id())) {
                this.company.id(Utils.guid());
            }
            JsonWebApi.post("Api/CompanyApi/Save", this.company, (responseData) => {
                if (!Utils.isEmptyOrWhitespace(responseData.error)) {
                    toastr.error(responseData.error, "Failed to Save Company");
                }
                else {
                    toastr.success(this.company.name(), "Saved");
                    this.search();
                    $("#editCompanyModal").modal("hide");
                }
            }, event);
        }

        erase = (data: Model.Company, event: Event) => {
            if (confirm(`Are you sure you want to delete ${this.company.name()}?`)) {
                JsonWebApi.post("Api/CompanyApi/Delete", this.company, (responseData) => {
                        if (!Utils.isEmptyOrWhitespace(responseData.error)) {
                            toastr.error(responseData.error, "Failed to delete company");
                        }
                        else {
                            toastr.success("Deleted");
                        }
                    this.search();
                        $("#editCompanyModal").modal("hide");
                }, event);
            }
        }
        
        //
        // -- private
        //

        private doSearch = Utils.debounce(() => {
            JsonWebApi.showWaiting(true);
            const parameters = ko.toJS(this.parameters);
            JsonWebApi.sendRequest(new JsonWebApi.Options({
                method: "POST",
                path: "Api/CompanyApi/Search",
                data: JSON.stringify(parameters),
                success: responseData => {
                    if (this.parameters.pageNumber === 1) {
                        this.totalCount(responseData.totalCount);
                    }
                    // don't push directly to the observable array as that would be slow!
                    const rawItems = this.companies();
                    for (let i = 0; i < responseData.companies.length; i++) {
                        const company = new Model.Company();
                        ko.mapping.fromJS(responseData.companies[i], Model.Company.mapping, company);
                        rawItems.push(company);
                    }
                    this.companies(rawItems);
                    this.parameters.lastPageLoaded = parameters.pageNumber;
                }, 
                always: () => {
                    JsonWebApi.showWaiting(false);
                }
            }));
        }, 200);
        
        private loadLookups = () => {
            JsonWebApi.get("Api/CompanyApi/GetLookups", (responseData) => {
                ko.mapping.fromJS(responseData.lookupOnes, Model.LookupItem.mapping, this.lookupOnes);
                ko.mapping.fromJS(responseData.lookupTwos, Model.LookupItem.mapping, this.lookupTwos);
                // get list of results after we have the lookups
                this.search();
            });           
        }
    }
}

