﻿namespace App.ViewModel {

    export class Users {

        constructor() {
            this.isEditingSelf = ko.computed({
                owner: this, read: () => {
                    return this.user.id() === appSettings.loggedInUserID;
                }
            });
            this.rowCountText = ko.computed({
                owner: this, read: () => {
                    return String(this.users().length);
                }
            });
            this.checkUrlForParameters();
            this.user.newPassword.subscribe(this.onPasswordChanged); 
        }
        
        //
        // -- properties
        //
        
        // lookups
        roles: KnockoutObservableArray<Model.LookupItem> = ko.observableArray([]);
        statuses: KnockoutObservableArray<Model.LookupItem> = ko.observableArray([]);

        // other vm properties
        parameters = new Model.UserSearchParameters();
        users: KnockoutObservableArray<Model.User> = ko.observableArray([]);
        user = new Model.User();

        // computed observables
        rowCountText: KnockoutComputed<string>;
        //isEditingSelf: KnockoutComputed<boolean>;
        isEditingSelf: KnockoutComputed<boolean> = ko.computed(() => this.user.id() === appSettings.loggedInUserID);

        selectedStatuses: KnockoutComputed<string> = ko.computed(() => {
            var text = "";
            var arrayLength = this.parameters.statuses().length;
            for (let i = 0; i < arrayLength; i++) {
                text += "id = " + this.parameters.statuses()[i] + "<br>";
            }
            return text;
        });

        canDelete: KnockoutComputed<boolean> = ko.computed(() => !Utils.isEmptyOrWhitespace(this.user.id()) && this.user.id() !== App.emptyGuidValue && (this.user.status() === UserStatus.Active || this.user.status() === UserStatus.WaitingEmailVerification));
        canAcceptOrReject: KnockoutComputed<boolean> = ko.computed(() => this.user.status() === UserStatus.WaitingApproval || this.user.status() === UserStatus.WaitingEmailVerification);
        canSave: KnockoutComputed<boolean> = ko.computed(() => this.user.status() === UserStatus.Active);

        //
        // -- methods
        //

        // called after DOM has loaded
        onLoad = () => {
            this.loadLookups();
        };

        search = () => {
            // at some point we may want to implement an 'infinite scroll' thing here...
            JsonWebApi.post("Api/UserApi/Search", this.parameters, (responseData) => {
                ko.mapping.fromJS(responseData, Model.User.mapping, this.users);
            }, event);
        }
        
        editNew = () => {
            // need to update currently bound object - replacing it screws up bindings
            this.user.reset();
            Validation.shouldShowMessages(this.user, false);
            $("#editUserModal").modal("show");
        }

        edit = (user: Model.User) => {
            JsonWebApi.get(`Api/UserApi/Load?id=${user.id()}`, (responseData) => {
                ko.mapping.fromJS(responseData, Model.User.mapping, this.user);
                Validation.shouldShowMessages(this.user, false);
                $("#editUserModal").modal("show");
            }, event);
        }

        cancelEdit = () => {
            $("#editUserModal").modal("hide");
            this.user.reset();
        }

        accept = (context: any, event: Event) => {
            this.user.status(UserStatus.Active);
            this.save(context, event);
        }

        reject = (context: any, event: Event) => {
            bootbox.prompt({
                title: "Please supply a rejection reason:",
                inputType: "textarea",
                callback: reasonText => {
                    if (Utils.isEmptyOrWhitespace(reasonText)) return;
                    this.user.registrationNotes(reasonText);
                    this.user.status(UserStatus.Deleted);
                    this.save(context, event);
                }
            });
        }

        save = (context: any, event: Event) => {
            Validation.shouldShowMessages(this.user, true);
            Validation.validate(this.user);
            if (!Validation.isValid(this.user)) {
                toastr.warning("Please fix the highlighted problems.", "Cannot Save");
                return;
            }

            JsonWebApi.post("Api/UserApi/Save", this.user, (responseData) => {
                if (!Utils.isEmptyOrWhitespace(responseData.error)) {
                    // TODO - just show validation message




                    toastr.error(responseData.error, "Failed to Save User");
                }
                else {
                    toastr.success(this.user.fullName(), "Saved");
                    this.search();
                    $("#editUserModal").modal("hide");
                }
            }, event);
        }

        deleteUser = (context: any, event: Event) => {
            if (confirm(`Are you sure you want to delete ${this.user.forename()}?`)) {
                JsonWebApi.post("Api/UserApi/Delete", this.user, (responseData) => {
                    if (!Utils.isEmptyOrWhitespace(responseData.error)) {
                        toastr.error(responseData.error, "Failed to delete user");
                    }
                    else {
                        toastr.success("Deleted");
                        this.search();
                        $("#editUserModal").modal("hide");
                    }
                }, event);
            }
        }

        //
        // -- private
        //

        private checkUrlForParameters = () => {
            var q = Utils.getParameterByName("q");
            if (q === "awaitingApproval") {
                // set parameters
                this.parameters.statuses([UserStatus.WaitingApproval]);
                // get rid of query string out of url
                window.history.replaceState({}, document.title, window.location.pathname);
            }
        }

        private loadLookups = () => {
            JsonWebApi.get("Api/UserApi/GetLookups", (responseData) => {
                ko.mapping.fromJS(responseData.roles.filter(e => { return e.id !== 0 }), Model.LookupItem.mapping, this.roles);
                ko.mapping.fromJS(responseData.statuses.filter(e => { return e.id !== 0 }), Model.LookupItem.mapping, this.statuses);
                this.search();
                // search when statuses change
                this.parameters.statuses.subscribe(this.search);
            });
        }

        private onPasswordChanged = Utils.debounce(() => {
            this.user.passwordServerValidationMessage("");
            // don't validate if new password is blank
            if (Utils.isEmptyOrWhitespace(this.user.newPassword())) return;
            // don't send pwd over insecure external network
            if (window.location.protocol !== "https:" && location.hostname !== "localhost") return;
            JsonWebApi.sendRequest(new JsonWebApi.Options({
                method: "GET",
                path: `Api/UserApi/ValidatePassword?password=${encodeURI(this.user.newPassword())}`,
                success: responseData => {
                    console.dir(responseData);
                    this.user.passwordServerValidationMessage(responseData.isOkay ? "" : responseData.message);
                }
            }));
        }, 200);
    }
}
