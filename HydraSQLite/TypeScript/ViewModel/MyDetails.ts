﻿namespace App.ViewModel {

    export class MyDetails {

        constructor() {
            this.setUpValidation();

            // trigger password validation when server message returns
            this.passwordServerValidationMessage.subscribe(() => {
                this.newPassword.validate();
            });
            this.newPassword.subscribe(this.onPasswordChanged);
        }

        //
        // -- properties
        //

        password: KnockoutObservable<string> = ko.observable("");
        newPassword: KnockoutObservable<string> = ko.observable("");
        repeatPassword: KnockoutObservable<string> = ko.observable("");
        passwordServerValidationMessage: KnockoutObservable<string> = ko.observable("");
        passwordValidationMessage: string = "";

        // lookups
        roles: KnockoutObservableArray<Model.LookupItem> = ko.observableArray([]);

        // other vm properties

        user = new Model.User();

        //
        // -- methods
        //

        // called after DOM has loaded
        onLoad = () => {
            this.loadLookups();
        };

        editPassword = () => {
            this.password("");
            this.newPassword("");
            this.repeatPassword("");
            Validation.shouldShowMessages(this, false);
            $("#editPasswordModal").modal("show");
        };

        cancelEditPassword = () => {
            $("#editPasswordModal").modal("hide");
        };

        savePassword = (data: Model.User, event: Event) => {
            Validation.shouldShowMessages(this, true);
            if (!Validation.isValid(this)) {
                toastr.warning("Please fix the highlighted problems.", "Cannot Save");
                return;
            }
            JsonWebApi.post("Api/UserApi/ChangePassword", {
                password: this.password(),
                newPassword: this.newPassword()
            }, (responseData) => {
                if (responseData.message !== "ok") {
                    // TODO - apply approraite validation indicators rather than toast?
                    
                    //this.password("");
                    //this.newPassword("");
                    //this.repeatPassword("");
                    //Validation.shouldShowMessages(this, false);

                    toastr.error(responseData.error, "Failed to Change Password");
                } else {
                    toastr.success("Password Changed");
                    $("#editPasswordModal").modal("hide");
                }
            }, event);
        };

        //
        // -- private
        //

        private loadUser() {
            JsonWebApi.get("Api/UserApi/LoggedInUser", (responseData) => {
                ko.mapping.fromJS(responseData, {}, this.user);
            });
        }

        private loadLookups() {
            JsonWebApi.get("Api/UserApi/GetLookups", (responseData) => {
                ko.mapping.fromJS(responseData.roles, Model.LookupItem.mapping, this.roles);
                // get list of results after we have the lookups
                this.loadUser();
            }, event);
        }

        // called from constructor
        private setUpValidation = () => {

            this.newPassword.extend({
                validate: {
                    rule: () => {
                        const isBlank = Utils.isEmptyOrWhitespace(this.newPassword());
                        const isTooShort = (!isBlank && this.newPassword().length < 8);
                        const isServerMessage = !Utils.isEmptyOrWhitespace(this.passwordServerValidationMessage());
                        this.passwordValidationMessage =
                            isServerMessage ? this.passwordServerValidationMessage() :
                            isBlank ? "Please enter a password" :
                            isTooShort ? "Password should be at least eight characters" : "";
                        return !(isBlank || isTooShort || isServerMessage);
                    },
                    message: () => {
                        return this.passwordValidationMessage;
                    }
                }
            });

            this.repeatPassword.extend({
                validate: {
                    rule: () => { return this.newPassword() === this.repeatPassword(); },
                    message: "Passwords must match"
                }
            });
        };
        
        private onPasswordChanged = Utils.debounce(() => {
            // don't send pwd over insecure external network
            if (window.location.protocol !== "https:" && location.hostname !== "localhost") return;
            JsonWebApi.sendRequest(new JsonWebApi.Options({
                method: "GET",
                path: `Api/UserApi/ValidatePassword?password=${encodeURI(this.newPassword())}`,
                success: responseData => {
                    this.passwordServerValidationMessage(responseData.isOkay ? "" : responseData.message);
                }
            }));
        }, 200);
    }
}
