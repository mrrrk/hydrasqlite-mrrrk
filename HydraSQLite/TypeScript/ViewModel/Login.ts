﻿namespace App.ViewModel {

    export class Login {

        constructor() {
            this.redirectUrl.subscribe(newValue => {
                console.log("Will redirect to " + newValue);
            });
        }

        //
        // -- properties
        //

        redirectUrl: KnockoutObservable<string> = ko.observable(""); // *** see: /Views/Index.ts 
        email: KnockoutObservable<string> = ko.observable("");
        password: KnockoutObservable<string> = ko.observable("");
        message: KnockoutObservable<string> = ko.observable("");
        forgotEmail: KnockoutObservable<string> = ko.observable("");
        forgotMessage: KnockoutObservable<string> = ko.observable("");
        isEmailFocus: KnockoutObservable<boolean> = ko.observable(true); // is set in onLoad

        // computed
        isMessage: KnockoutComputed<boolean> = ko.computed({
            owner: this, read: () => {
                return this.message() !== "" && this.message().indexOf("ok") !== 0;
            }
        });

        isForgotMessage: KnockoutComputed<boolean> = ko.computed({
            owner: this, read: () => {
                return this.forgotMessage() !== "" && this.forgotMessage().indexOf("ok") !== 0;
            }
        });
        
        //
        // -- methods
        //

        // called after DOM has loaded
        onLoad = () => {
            // previous email?
            if (typeof (Storage) !== "undefined") {
                const userJson = localStorage.getItem("user");
                if (userJson) {
                    try {
                        const user = JSON.parse(userJson);
                        this.email((user && user.email) ? user.email : "");
                    } catch (e) {
                        this.email("");
                    }
                }
            }
            this.isEmailFocus(Utils.isEmptyOrWhitespace(this.email()));

            $("#checkEmailAndPasswordFailedModal").on("shown.bs.modal", () => {
                $("#failedModalBody").focus(); // focus allows dismiss with keypress
            }).on("hidden.bs.modal", () => {
                // make sure right bit has focus
                $("#failedModalBody").blur();
                const isEmail = !Utils.isEmptyOrWhitespace(this.email());
                this.isEmailFocus(isEmail);
                this.isEmailFocus(!isEmail);
            });
        }

        login = (data: any, event: Event) => {
            if (appSettings.isDigestAuthenticationScheme) {
                this.digestLoginStart(event, this.email(), this.password());
            }
            else {
                this.secureLogin(event, this.email(), this.password());
            }
        }

        testLogin = (data: any, event: Event) => {
            var button: HTMLElement = event.target as HTMLElement;
            var email = button.getAttribute("data-email");
            var password = button.getAttribute("data-password");
            if (appSettings.isDigestAuthenticationScheme) {
                this.digestLoginStart(event, email, password);
            }
            else {
                this.secureLogin(event, email, password);
            }
        }

        forgot = (data: any, event: Event) => {
            const postData = {
                email: this.forgotEmail()
            };
            JsonWebApi.post("Api/LoginApi/Forgot", postData, (responseData) => {
                    this.forgotMessage(responseData.message || "");
            }, event);
        }

        closeFailedModal = () => {
            var dlg = $("#checkEmailAndPasswordFailedModal");
            if (dlg && dlg.length > 0) {
                dlg.modal("hide");
            }
        }

        //
        // -- private 
        //

        private secureLogin = (event: Event, email: string, password: string) => {
            JsonWebApi.post("Api/LoginApi/SecureLogin", {
                email: email,
                password: password,
                redirectUrl: this.redirectUrl()
            }, this.loginDone, event);
        }

        private digestLoginStart = (event: Event, email: string, password: string) => {
            JsonWebApi.get(`Api/LoginApi/ServerNOnce?user=${email}`, (responseData) => {
                    this.message(responseData.message || "");
                    if (responseData.message === "ok") {
                        this.digestLogin(event, email, password, responseData.serverNOnce);
                    }
                    else {
                        // the modal is not present except on signedout homepage
                        const dlg = $("#checkEmailAndPasswordFailedModal");
                        if (dlg && dlg.length > 0) {
                            dlg.modal("show");
                        }
                    }
            }, event);
        }

        private digestLogin = (event: Event, email: string, password: string, serverNOnce: string) => {
            const cnonce = this.clientNOnce();
            JsonWebApi.post("Api/LoginApi/DigestLogin", {
                email: email,
                cnonce: cnonce,
                digest: Sha256.hash(serverNOnce + cnonce + password),
                redirectUrl: this.redirectUrl()
            }, this.loginDone, event);
        }

        private clientNOnce = (): string => {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                }
            return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
        }

        private loginDone = (responseData:any) => {
            this.message(responseData.message);
            if (this.message() === "ok") {
                // save user data
                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem("user", JSON.stringify(responseData.user));
                }
                if (responseData.redirectUrl && responseData.redirectUrl.length !== 0) {
                    window.location.replace(responseData.redirectUrl);
                }

                // hide dialogue and clean up (if is login dialogue)

                const loginDlg = $("#sessionTimeoutModal");
                if (loginDlg && loginDlg.length > 0) {
                    loginDlg.modal({
                        backdrop: "none",
                        show: false
                    });
                    // backdrop not disappearing? - this is such a hack...
                    const backdrops = $(".modal-backdrop");
                    if (backdrops.length === 1) {
                        $("body").removeClass("modal-open");
                        backdrops.remove();
                    }
                    else if (backdrops.length > 1) {
                        // other modals open beneath?
                        backdrops.last().remove();
                    }
                }
                const dlgContainer = document.getElementById("sessionTimeoutModalContainer");
                if (dlgContainer) {
                    ko.cleanNode(dlgContainer);
                    dlgContainer.innerHTML = "";
                }
                App.isShowingTimeoutLoginDialogue = false;

                // re-run any failed requests
                JsonWebApi.onLoggedInCallback();
            }
            else {
                // the modal is not present except on signedout homepage
                const failDlg = $("#checkEmailAndPasswordFailedModal");
                if (failDlg && failDlg.length > 0) {
                    failDlg.modal("show");
                }
            }
        }
    }
}

