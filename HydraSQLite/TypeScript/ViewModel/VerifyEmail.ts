﻿namespace App.ViewModel {

    export class VerifyEmail {

        messageHeading: KnockoutObservable<string> = ko.observable("- - -");
        messageBody: KnockoutObservable<string> = ko.observable("- - -");

        messageCalloutCss: KnockoutObservable<string> = ko.observable("callout");

        // called after DOM has loaded
        onLoad = () => {
            const pathParts = window.location.pathname.split("/");
            if (pathParts.length < 3) {
                this.messageCalloutCss("callout callout-danger");
                this.messageHeading("Error");
                this.messageBody("That does not seem to be a valid URL.  Please try clicking the link again, re-sending the verification or submitting a new Create Account request.");
            }
            var key = pathParts[pathParts.length - 1];
            this.verifyEmail(key);
        };

        //
        // -- private
        //
        
        private verifyEmail = (key: string) => {
            JsonWebApi.get(`Api/CreateAccountApi/VerifyEmail?key=${key}`, (responseData) => {
                if (responseData.keyFailed) {
                    this.messageCalloutCss("callout callout-danger");
                    this.messageHeading("Link Expired");
                    this.messageBody("The link has expired or possibly it has not been entered correctly.  Please try following the link again, re-sending the verification or submitting a new Create Account request.");
                }
                else if (responseData.unexpectedUserStatus) {
                    this.messageCalloutCss("callout callout-success");
                    this.messageHeading("Already Verified");
                    this.messageBody("There was no email verification pending for that link.  Perhaps you've already verified it?");
                }
                else if (responseData.isOk) {
                    this.messageCalloutCss("callout callout-success");
                    this.messageHeading("Success");
                    this.messageBody("Thank you.  Your details will now need to be approved by one of out administrators, so please be patient while this happens.");
                }
            });
        }
    }
}