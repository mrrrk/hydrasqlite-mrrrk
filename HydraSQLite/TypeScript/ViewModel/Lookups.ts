﻿namespace App.ViewModel {

    export class Lookups {
        
        //
        // -- properties
        //

        lookupItems: KnockoutObservableArray<Model.LookupItem> = ko.observableArray([]);
        lookup = new Model.LookupItem();
        lookupGroup = "";

        // computed observables
        rowCountText: KnockoutComputed<string> = ko.computed({
            owner: this, read: () => {
                return String(this.lookupItems().length);
            }
        });

        //
        // -- methods
        //

        // call after DOM has loaded
        onLoad = () => {
            this.search();
        }

        search = () => {
            JsonWebApi.get(`Api/LookupApi/AllLookupItems/${this.lookupGroup}`, (data) => {
                ko.mapping.fromJS(data, {}, this.lookupItems);
            });
        }

        // could go to server for this - but will assume data here is up-to-date
        getItem = (id: number) => {
            var match = ko.utils.arrayFirst(this.lookupItems(), item => (id === item.id()));
            return match;
        };

        editNew = () => {
            this.lookup.reset();
            Validation.shouldShowMessages(this.lookup, false);
            $("#editLookupModal").modal("show");
        };

        edit = (lookup: Model.LookupItem) => {
            ko.mapping.fromJS(ko.toJS(this.getItem(lookup.id())), {}, this.lookup);
            Validation.shouldShowMessages(this.lookup, false);
            $("#editLookupModal").modal("show");
        };

        cancelEdit = () => {
            $("#editLookupModal").modal("hide");
            this.lookup.reset();
        };

        save = (data: Model.LookupItem, event: Event) => {
            Validation.shouldShowMessages(this.lookup, true);
            Validation.validate(this.lookup);
            if (!Validation.isValid(this.lookup)) {
                toastr.warning("Please fix the highlighted problems.", "Cannot Save");
                return;
            }

            JsonWebApi.post(`Api/LookupApi/Save/${this.lookupGroup}`, this.lookup, (responseData) => {
                if (!Utils.isEmptyOrWhitespace(responseData.error)) {
                    toastr.error(responseData.error, "Failed to Save Lookup");
                }
                else {
                    toastr.success(this.lookup.description(), "Saved");
                    this.search();
                    $("#editLookupModal").modal("hide");
                }
            }, event);
        };

        erase = (data: Model.User, event: Event) => {
            if (confirm(`Are you sure you want to delete ${this.lookup.description()}?`)) {
                JsonWebApi.post(`Api/LookupApi/Delete/${this.lookupGroup}`, this.lookup, (responseData) => {
                    toastr.success(this.lookup.description(), responseData.wasDeleted ? "Deleted" : "Archived");
                    this.lookup.reset();
                    $("#editLookupModal").modal("hide");
                    this.search(); // reload 
                }, event);
            }
        };
    }
}