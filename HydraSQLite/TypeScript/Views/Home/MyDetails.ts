﻿var myDetailsViewModel = new App.ViewModel.MyDetails();

// this is like jquery ready function
// (not supported in IE8 - but who gives...)
document.addEventListener("DOMContentLoaded", (): void => {

    myDetailsViewModel.onLoad();

    ko.applyBindings(myDetailsViewModel, document.getElementById("koContentBindingNode"));

    // handler to clear out object after editing...
    $("#editPasswordModal").on("hidden.bs.modal", () => {
        myDetailsViewModel.password("");
        myDetailsViewModel.newPassword("");
        myDetailsViewModel.repeatPassword("");
    });

    //// menu select
    $("#menuMyDetails").addClass("active");

});