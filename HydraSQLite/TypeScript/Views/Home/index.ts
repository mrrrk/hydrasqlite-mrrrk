﻿document.addEventListener("DOMContentLoaded", (): void => {

    $("#menuHome").addClass("active");

    // Not calling any APIs so need to hide the blackout div manually.
    App.JsonWebApi.showWaiting(false);
});

