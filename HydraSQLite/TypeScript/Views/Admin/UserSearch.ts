﻿var usersViewModel = new App.ViewModel.Users();

// this is like jquery ready function
// (not supported in IE8 - but who gives...)
document.addEventListener("DOMContentLoaded", (): void => {

    usersViewModel.onLoad();

    ko.applyBindings(usersViewModel, document.getElementById("koContentBindingNode"));

    // handler to clear out object after editing...
    $("#editUserModal").on("hidden.bs.modal", () => {
        usersViewModel.user.reset();
    });

    // menu select
    $("#menuAdmin").addClass("active");
    $("#menuAdmin").collapse("show");

});