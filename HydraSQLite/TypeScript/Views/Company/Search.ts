﻿var companySearchViewModel = new App.ViewModel.CompanySearch();

document.addEventListener("DOMContentLoaded", (): void => {

    companySearchViewModel.onLoad();

    ko.applyBindings(companySearchViewModel, document.getElementById("koContentBindingNode"));

    //Endless scrolling event
    const win = $(window as any);
    const doc = $(window.document as any);
    win.scroll(() => {
        var scrollTopPlusHeight = +win.scrollTop() + +win.height();
        var threshold = doc.height() - 80;
        if (scrollTopPlusHeight > threshold) {
            companySearchViewModel.loadNextPage();
        }
    });

    // menu select
    $("#menuCompanies").addClass("active");

});