﻿var resetPasswordViewModel = new App.ViewModel.ResetPassword();

document.addEventListener("DOMContentLoaded", (): void => {
    resetPasswordViewModel.onLoad();
    ko.applyBindings(resetPasswordViewModel);
});