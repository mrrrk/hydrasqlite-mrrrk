﻿var loginViewModel = new App.ViewModel.Login();
var createAccountViewModel = new App.ViewModel.CreateAccount();

document.addEventListener("DOMContentLoaded", (): void => {
    loginViewModel.redirectUrl(appSettings.rootUrl + "Company/Search");
    
    ko.applyBindings(loginViewModel, document.getElementById("koLoginBindingNode"));
    ko.applyBindings(createAccountViewModel, document.getElementById("koCreateAccountBindingNode"));

    loginViewModel.onLoad();
    createAccountViewModel.onLoad();
});