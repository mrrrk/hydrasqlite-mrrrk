﻿module App.ErrorHandler {

    export function reportJavascriptError(
        message: string,
        source: string,
        lineno: number,
        colno: number,
        error: Error) {

        postJavascriptError(message, source, lineno, colno, error);
        toastr.error("Report sent", "Unexpected Error!");
    };

    //
    // -- private
    //

    function postJavascriptError(
        message: string,
        source: string,
        lineno: number,
        colno: number,
        error: Error) {

        // TODO - use stack-mapper ? - no, we need map files for bundles - can this be done server side?
        //const inframes = [{
        //    filename: source,
        //    line: lineno,
        //    column: colno
        //}];
        //const map = {};
        //const sm: stackMapper.StackMapper = stackMapper(map);
        //const frames = sm.map(inframes);

        // --- 

        const text = "Message : " + message +
            "\nSource  : " + source + // frames[0].filename +
            "\nLine    : " + lineno + // frames[0].line +
            "\nCol     : " + colno + // frames[0].column +
            "\nError   : " + JSON.stringify(error);

        const report = {
            Application: "HydraSQLite",  // overrwitten on server! (web.config value)
            FullText: text,
            ExceptionType: "JavaScript Error",
            Location: source + " at line " + lineno
        };
        postErrorReport(report);
    };

    function postErrorReport(report) {
        try {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", appSettings.rootUrl + "Api/ErrorReportApi/PostErrorReport", true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    console.log("error report sent:" + xhr.responseText);
                }
                else if (xhr.readyState === 4 && xhr.status !== 200) {
                    console.log("failed to send error report - status:" + xhr.status);
                }
            }
            xhr.send(JSON.stringify(report));
        }
        catch (e) {
            console.log("failed to send error report");
        }
    };

    
}
