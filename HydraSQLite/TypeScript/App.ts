﻿//
//  - use this only for global enums and constants!
//

module App {

    export const emptyDateValue = -62135596800000;

    export const emptyGuidValue = "00000000-0000-0000-0000-000000000000";

    export let isShowingTimeoutLoginDialogue: boolean = false;
    
    export enum ValidationType {
        Recursive = 0,
        Shallow = 1
    }

    export enum UserRole {
        None = 0,
        Admin = 1,
        Basic = 2
    }

    export enum UserStatus {
        None = 0,
        WaitingEmailVerification = 1,
        WaitingApproval = 2,
        Active = 10,
        Deleted = 20
    }
}
