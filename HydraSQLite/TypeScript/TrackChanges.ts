﻿ko.extenders.trackChange = (target, track) => {
    if (track) {
        target.isDirty = ko.observable(false);
        target.originalValue = target();
        //console.log("*** setting up ko.extenders.trackChange - orig = " + target.originalValue);
        target.subscribe(newValue => {
            target.isDirty(newValue !== target.originalValue);
            if (target.isDirty) {
                console.log(`*** changed from '${target.originalValue}' to '${newValue}'`);
            }
        });
    }
    return target;
};
