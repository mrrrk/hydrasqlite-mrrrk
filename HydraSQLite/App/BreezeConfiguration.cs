﻿using System;
using System.Configuration;

namespace HydraSQLite.App {

    public interface IBreezeConfiguration {
        string ConnectionString();
        bool IsDigestAuthenticationScheme { get; }
        string BasiliskUrl { get; }
        string BasiliskAppName { get; }
        string SmtpServer { get; }
        string AutoEmailFrom { get; }
        TimeSpan ShortCachePeriod { get; }
        bool IsStaging { get; }
        int EndlessScrollPageSize { get; }
        TimeSpan PasswordReminderCachePeriod { get; }
        TimeSpan EmailVerificationCachePeriod { get; }
    }
    
    // TODO - cache all the settings?

    public class BreezeConfiguration : ConfigurationSection, IBreezeConfiguration {

        private static readonly int myShortCachePeriodSeconds;
        private static readonly int myEndlessScrollPageSize;
        private static readonly int myPasswordReminderCachePeriodHours;
        private static readonly int myEmailVerificationCachePeriodHours;

        static BreezeConfiguration() {
            var appSettings = ConfigurationManager.AppSettings;
            myShortCachePeriodSeconds = appSettings["ShortCachePeriodSeconds"].ToInt();
            myEndlessScrollPageSize = appSettings["EndlessScrollPageSize"].ToInt();
            myPasswordReminderCachePeriodHours = appSettings["PasswordReminderCachePeriodHours"].ToInt();
            myEmailVerificationCachePeriodHours = appSettings["EmailVerificationCachePeriodHours"].ToInt();
        }

        private static BreezeConfiguration Attributes { get; } = ConfigurationManager.GetSection("breezeConfiguration") as BreezeConfiguration;

        //
        // -- breezeConfiguration section
        //

        [ConfigurationProperty("databasePath", IsRequired = true)]
        public string DatabasePath => (string)Attributes["databasePath"];

        public string ConnectionString() {
            var path = DatabasePath;
            if (path.StartsWith("~")) {
                path = System.Web.Hosting.HostingEnvironment.MapPath(path);
            }

            // This is a writable connection - which will *lock* the table during queries!
            //  - this is only for demo purposes as it would normally be a SQL Server connection

            return $"Data Source={path};Version=3;";

            // return $"Data Source={path};Version=3;Read Only=True";
        }

        [ConfigurationProperty("isDigestAuthenticationScheme", IsRequired = true)]
        public bool IsDigestAuthenticationScheme => (bool)Attributes["isDigestAuthenticationScheme"];
        
        [ConfigurationProperty("basiliskUrl", IsRequired = true)]
        public string BasiliskUrl => (string)Attributes["basiliskUrl"];

        [ConfigurationProperty("basiliskAppName", IsRequired = true)]
        public string BasiliskAppName => (string)Attributes["basiliskAppName"];

        [ConfigurationProperty("smtpServer", IsRequired = true)]
        public string SmtpServer => (string)Attributes["smtpServer"];

        [ConfigurationProperty("autoEmailFrom", IsRequired = true)]
        public string AutoEmailFrom => (string)Attributes["autoEmailFrom"];

        //
        // -- appSettings section
        //

        public TimeSpan ShortCachePeriod => TimeSpan.FromSeconds(myShortCachePeriodSeconds);

        // TODO - in Hydra, we can tell it's staging from the connection string name - but not in the SQLite version
        public bool IsStaging => false;

        public int EndlessScrollPageSize => myEndlessScrollPageSize;
        
        public TimeSpan PasswordReminderCachePeriod => TimeSpan.FromHours(myPasswordReminderCachePeriodHours);

        public TimeSpan EmailVerificationCachePeriod => TimeSpan.FromHours(myEmailVerificationCachePeriodHours);
    }
}
