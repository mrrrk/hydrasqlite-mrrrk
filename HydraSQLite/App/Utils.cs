﻿using System;

namespace HydraSQLite.App {

    public static class Utils {

        public static bool IsDebug() {
#if DEBUG
            return true;
#else
            return false;
#endif
        }

        public static Guid NewGuid() {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var now = DateTime.UtcNow;
            var javaTime = Convert.ToInt64((now - epoch).TotalMilliseconds);
            var timeBytes = BitConverter.GetBytes(javaTime);
            var randomBytes = new byte[10];
            new Random(now.Millisecond).NextBytes(randomBytes);
            byte mask = 0x0F;
            byte version = 0x40;
            version = (byte)((randomBytes[7] & mask) | version);
            var bytes = new byte[] {
                randomBytes[0], randomBytes[1], randomBytes[2], randomBytes[3],
                randomBytes[4], randomBytes[5], randomBytes[6], version,
                randomBytes[8], randomBytes[9], timeBytes[5], timeBytes[4],
                timeBytes[3], timeBytes[2], timeBytes[1], timeBytes[0]
            };
            return new Guid(bytes);
        }
    }
}