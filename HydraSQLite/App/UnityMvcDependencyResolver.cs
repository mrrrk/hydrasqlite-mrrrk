﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Unity;

namespace HydraSQLite.App {

    public class UnityMvcDependencyResolver : IDependencyResolver {

        private readonly IUnityContainer myContainer;

        public UnityMvcDependencyResolver(IUnityContainer container) {
            myContainer = container;
        }

        public object GetService(Type serviceType) {
            if (typeof(IController).IsAssignableFrom(serviceType)) {
                return myContainer.Resolve(serviceType);
            }
            return myContainer.IsRegistered(serviceType) ? myContainer.Resolve(serviceType) : null;
        }

        public IEnumerable<object> GetServices(Type serviceType) {
            return myContainer.ResolveAll(serviceType);
        }
    }
}