﻿using System.Linq;
using HydraSQLite.Model;

namespace HydraSQLite.App {
    public static class AuthoriseRoleChecker {

        public static bool IsInRole(User user, UserRole[] acceptedRoles) {
            // if no roles just assume anything goes
            if (acceptedRoles == null || acceptedRoles.Length == 0) return true;
            if (user == null) return false;
            // admin user can do anything - otherwise look for match
            return user.Role == UserRole.Admin || acceptedRoles.Any(r => r == user.Role);
        }
    }
}