﻿using System.Web.Optimization;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(HydraSQLite.BundleConfig), "RegisterBundles")]

namespace HydraSQLite {

    public class BundleConfig {

        public static void RegisterBundles() {

            //
            // -- CSS
            //
            
            // NB - FontAwesome CSS is separate 'cos it seems to break otherwise :-(
            BundleTable.Bundles.Add(new StyleBundle("~/assets/lib/font-awesome/css/fontawesome").Include("~/Assets/lib/font-awesome/css/font-awesome.css"));           
            BundleTable.Bundles.Add(new StyleBundle("~/assets/css/site").Include(
                "~/Assets/lib/bootstrap/css/bootstrap.css",
                "~/Assets/lib/toastr/toastr.css",
                "~/Assets/css/smalot-datetimepicker/bootstrap-datetimepicker.css",
                "~/Assets/css/site.css"));


            //
            // -- general scripts
            //

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/commonLibraryScripts").Include(
                "~/Assets/lib/jquery/jquery.js",
                "~/Assets/lib/bootstrap/js/bootstrap.js",
                "~/Assets/lib//smalot-datetimepicker/bootstrap-datetimepicker.js",
                "~/Assets/lib/bootbox/bootbox.js",
                "~/Assets/lib/knockout/knockout-latest.debug.js",
                "~/Assets/lib/knockout-mapping/knockout.mapping.js",
                "~/Assets/lib/toastr/toastr.js",
                "~/Assets/lib/sha256/sha256.js",
                "~/Assets/lib/spin/spin.js",
                "~/Assets/lib/moment/moment.js",
                "~/Assets/js/App.js",
                "~/Assets/js/ErrorHandler.js",
                "~/Assets/js/Utils.js",
                "~/Assets/js/JsonWebApi.js",
                "~/Assets/js/CustomBindings.js",
                "~/Assets/js/trackChanges.js",
                "~/Assets/js/validation.js",
                "~/Assets/js/Model/LookupItem.js",
                "~/Assets/js/Model/User.js",
                "~/Assets/js/ViewModel/Login.js"
            ));
            
            //
            // -- signed out scripts and css
            //
            
            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/signedOutIndexScripts").Include(
                "~/Assets/js/Model/UserToCreate.js",
                "~/Assets/js/ViewModel/CreateAccount.js",
                "~/Assets/js/Views/SignedOut/Index.js"
            ));

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/signedOutResetPasswordScripts").Include(
                "~/Assets/js/ViewModel/ResetPassword.js",
                "~/Assets/js/Views/SignedOut/ResetPassword.js"
            ));

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/signedOutVerifyEmailScripts").Include(
                "~/Assets/js/ViewModel/VerifyEmail.js",
                "~/Assets/js/Views/SignedOut/VerifyEmail.js"
            ));

            //
            // -- layout script (menu, etc.)
            //

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/layoutScripts").Include(
                "~/Assets/js/Views/Shared/Layout.js"
            ));

            //
            // -- home scripts
            //

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/homeIndexScripts").Include(
                "~/Assets/js/Views/Home/Index.js"
            ));

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/homeMyDetailsScripts").Include(
                "~/Assets/js/ViewModel/MyDetails.js",
                "~/Assets/js/Views/Home/MyDetails.js"
            ));

            //
            // -- admin scripts
            //

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/adminUsersScripts").Include(
                "~/Assets/js/Model/UserSearchParameters.js",
                "~/Assets/js/ViewModel/Users.js",
                "~/Assets/js/Views/Admin/Users.js"
            ));

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/adminLookupsScripts").Include(
                "~/Assets/js/ViewModel/Lookups.js",
                "~/Assets/js/Views/Admin/Lookups.js"
            ));

            //
            // -- company scripts
            //

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/companySearchScripts").Include(
                "~/Assets/js/Model/Company.js",
                "~/Assets/js/Model/CompanySearchParameters.js",
                "~/Assets/js/ViewModel/CompanySearch.js",
                "~/Assets/js/Views/Company/Search.js"
            ));
        }
    }
}