﻿using System.Web.Http;
using HydraSQLite.App;
using Unity;

namespace HydraSQLite {

    public static class WebApiConfig {
        
        public static void Register(HttpConfiguration config) {

            config.EnableCors();

            config.Routes.MapHttpRoute(
                name: "LookupApi",
                routeTemplate: "api/LookupApiController/{action}/{lookupID}/{id}",
                defaults: new {
                    id = RouteParameter.Optional
                }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // dependency injection
            //   - See UnityConfig for list of registered services
            config.DependencyResolver = new UnityWebApiDependencyResolver(UnityConfig.Container);
            
            // API error reporter
            var breezeConfig = UnityConfig.Container.Resolve<IBreezeConfiguration>();
            config.Filters.Add(new UnhandledExceptionFilter(breezeConfig));

        }
    }
}
