using System.Linq;
using System.Web.Mvc;
using Unity.AspNet.Mvc;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(HydraSQLite.UnityMvcActivator), nameof(HydraSQLite.UnityMvcActivator.Start))]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(HydraSQLite.UnityMvcActivator), nameof(HydraSQLite.UnityMvcActivator.Shutdown))]

namespace HydraSQLite {

    /// <summary>
    /// Provides the bootstrapping for integrating Unity with ASP.NET MVC.
    ///   - See WebApiConfig for the Web API side of it
    ///   - List of services (types) is set in UnityConfig
    ///   - Both MVC and Web API use the same UnityConfig
    /// </summary>
    public static class UnityMvcActivator {

        /// <summary>
        /// Integrates Unity when the application starts.
        /// </summary>
        public static void Start() {

            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(UnityConfig.Container));

            DependencyResolver.SetResolver(new App.UnityMvcDependencyResolver(UnityConfig.Container));
        }

        /// <summary>
        /// Disposes the Unity container when the application is shut down.
        /// </summary>
        public static void Shutdown() {
            UnityConfig.Container.Dispose();
        }
    }
}