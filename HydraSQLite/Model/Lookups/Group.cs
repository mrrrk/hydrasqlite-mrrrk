﻿using System.ComponentModel;

namespace HydraSQLite.Model.Lookups {

    public enum Group {
        All = 0,
        [Description("The first lookup")]
        LookupOne = 1,
        [Description("The second lookup")]
        LookupTwo = 2
    }
}