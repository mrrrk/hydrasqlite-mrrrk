﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Lookups {

    public class Item {

        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("displayOrder")]
        public int DisplayOrder { get; set; }

        [JsonProperty("isArchived")]
        public bool IsArchived { get; set; }
    }
}