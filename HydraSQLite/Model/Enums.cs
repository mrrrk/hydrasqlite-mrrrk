﻿using System.ComponentModel;

namespace HydraSQLite.Model {

        public enum UserRole {
            LoggedOut = 0,
            [Description("User with administration rights")]
            Admin = 1,
            [Description("Basic user")]
            Basic = 2
        }

        public enum UserStatus {
            None = 0,
            [Description("Awaiting email verification")]
            WaitingEmailVerification = 1,
            [Description("Awaiting approval")]
            WaitingApproval = 2,
            Active = 10,
            Deleted = 20
        }
}