﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class UserSearchParameters {

        private string myText = "";

        [JsonIgnore]
        public string Email { get; set; } = "";

        [JsonProperty("text")]
        public string Text {
            get => myText;
            set => myText = value?.Replace("%", "").Replace(";", "") ?? "";
        }

        [JsonProperty("statuses")]
        public UserStatus[] Statuses { get; set; } = { };
    }
}