﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {
    public class CompanySearchParameters {

        public enum SortFieldType {
            Name,
            Town,
            County,
            PostCode,
            LastUpdated,
            Created,
            LookupOne,
            LookupTwo
        }

        private string mySearchText = "";

        [JsonProperty("searchText")]
        public string SearchText {
            get => mySearchText;
            set => mySearchText = value?.Replace("'", "''").Replace("%", "").Replace(";", "") ?? "";
        }

        [JsonProperty("pageNumber")]
        public int PageNumber { get; set; } = 1;

        // serialise as string - not number
        [JsonProperty("sortField"), JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public SortFieldType SortField { get; set; } = SortFieldType.Name;

        [JsonProperty("isSortAscending")]
        public bool IsSortAscending { get; set; } = true;

        [JsonIgnore]
        public string SortSqlColumn {
            get {
                switch (SortField) {
                    case SortFieldType.Name: return "cmpName";
                    case SortFieldType.Town: return "cmpTown";
                    case SortFieldType.County: return "cmpCounty";
                    case SortFieldType.PostCode: return "cmpPostCode";
                    case SortFieldType.Created: return "cmpCreated";
                    case SortFieldType.LastUpdated: return "cmpLastUpdated";
                    case SortFieldType.LookupOne: return "cmpLookupOneID";
                    case SortFieldType.LookupTwo: return "cmpLookupTwoID";
                    default: return "cmpCreated";
                }
            }
        }

        public override string ToString() {
            return $@"SearchText=""{SearchText}"", PageNumber={PageNumber}";
        }
    }
}