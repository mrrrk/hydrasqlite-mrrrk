﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class ForgotRequest {

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}