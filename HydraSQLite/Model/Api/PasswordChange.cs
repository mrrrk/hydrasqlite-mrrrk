﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class PasswordChange {

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }
    }
}