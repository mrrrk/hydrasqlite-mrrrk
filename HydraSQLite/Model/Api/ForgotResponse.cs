﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class ForgotResponse {

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}