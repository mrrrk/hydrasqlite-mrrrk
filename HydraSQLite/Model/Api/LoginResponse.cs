﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class LoginResponse {

        [JsonProperty("redirectUrl")]
        public string RedirectUrl { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("isSuccess")]
        public bool IsSuccess => Message == "ok";

    }
}