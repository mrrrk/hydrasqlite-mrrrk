﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class LoginRequest {
   
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("cnonce")]
        public string ClientNOnce { get; set; }
        
        [JsonProperty("digest")]
        public string Digest { get; set; }

        [JsonProperty("redirectUrl")]
        public string RedirectUrl { get; set; }
    }
}