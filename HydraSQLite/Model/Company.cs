﻿using System;
using Newtonsoft.Json;

namespace HydraSQLite.Model {

    public class Company {

        [JsonProperty("id")]
        public Guid ID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [JsonProperty("town")]
        public string Town { get; set; }

        [JsonProperty("county")]
        public string County { get; set; }

        [JsonProperty("postcode")]
        public string PostCode { get; set; }

        [JsonProperty("dateEstablished")]
        public DateTime DateEstablished { get; set; }
        
        [JsonProperty("lookupOneID")]
        public int LookupOneID { get; set; }

        [JsonProperty("lookupTwoID")]
        public int LookupTwoID { get; set; }

        [JsonProperty("created")]
        public DateTime Created { get; set; }

        [JsonProperty("createdByUserID")]
        public Guid CreatedByUserID { get; set; }

        [JsonProperty("lastUpdated")]
        public DateTime LastUpdated { get; set; }

        [JsonProperty("lastUpdatedByUserID")]
        public Guid LastUpdatedByUserID { get; set; }

    }
}