﻿using System;

namespace Tests.XUnitExtensions {

    public class SequenceAttribute : Attribute {

        public SequenceAttribute(int sequence) {
            Sequence = sequence;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public int Sequence { get; }
    }
}
