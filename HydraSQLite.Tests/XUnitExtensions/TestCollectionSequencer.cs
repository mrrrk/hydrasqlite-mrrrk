﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xunit.Abstractions;

namespace Tests.XUnitExtensions {

    // from http://www.tomdupont.net/2016/04/how-to-order-xunit-tests-and-collections.html
    public class TestCollectionSequencer {

        public IEnumerable<ITestCollection> OrderTestCollections(IEnumerable<ITestCollection> testCollections) {
            return testCollections.OrderBy(GetOrder);
        }

        /// <summary>
        /// Test collections are not bound to a specific class, however they
        /// are named by default with the type name as a suffix. We try to
        /// get the class name from the DisplayName and then use reflection to
        /// find the class and SequenceAttribute.
        /// </summary>
        private static int GetOrder(ITestCollection testCollection) {
            var name = testCollection.DisplayName;
            System.Diagnostics.Debug.WriteLine("******  testCollection.DisplayName " + name);
            Console.WriteLine("******  testCollection.DisplayName " + name);
            var i = name.LastIndexOf(' ');
            if (i <= -1) {
                System.Diagnostics.Debug.WriteLine("***  testCollection.DisplayName - tilt 1");
                return 0;
            }
            var className = testCollection.DisplayName.Substring(i + 1);
            System.Diagnostics.Debug.WriteLine("***  className " + className);
            var type = Type.GetType(className);
            if (type == null) {
                System.Diagnostics.Debug.WriteLine("***  testCollection.DisplayName - tilt 2");
                return 0;
            }
            var attr = type.GetCustomAttribute<SequenceAttribute>();
            System.Diagnostics.Debug.WriteLine("***  attr:" + attr?.Sequence.ToString() ?? "null");
            return attr?.Sequence ?? 0;
        }
    }
}
