﻿using Xunit;

namespace Tests.XUnitExtensions {

    // from http://www.tomdupont.net/2016/04/how-to-order-xunit-tests-and-collections.html
    [TestCaseOrderer("Tests.XUnitExtensions.TestCaseSequencer", "Tests")]
    public class SequencedTest {

        protected static int Sequence;

        protected void AssertTestName(string testName) {
            var type = GetType();
            var queue = TestCaseSequencer.QueuedTests[type.FullName ?? ""];
            var result = queue.TryDequeue(out var dequeuedName);
            Assert.True(result);
            Assert.Equal(testName, dequeuedName);
        }
    }
}
