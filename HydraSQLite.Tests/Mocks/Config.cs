﻿using System;
using HydraSQLite.App;
using System.IO;

namespace Tests.Mocks {

    // represents settings that would come from web.config
    public class Config : IBreezeConfiguration {

        public string ConnectionString() {
            // try to figure out where the SQLite database is based on where tests are running from...
            var appPath = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            var binFolder = Path.GetDirectoryName(appPath);
            var solutionFolder = Directory.GetParent(binFolder).Parent;           
            return $"Data Source={solutionFolder?.FullName}\\HydraSQLite\\App_Data\\Hydra.s3db;Version=3;";
        }

        public bool IsDigestAuthenticationScheme => true;
        public string BasiliskUrl => "http://url.for.error.reporting.to.nowhere";
        public string BasiliskAppName => "Test";
        public string SmtpServer => "localhost";
        public string AutoEmailFrom => "test@breeze-it.com";
        public TimeSpan ShortCachePeriod => new TimeSpan(0, 0, 1);
        public bool IsStaging => false;
        public int EndlessScrollPageSize => 10;
        public TimeSpan PasswordReminderCachePeriod => new TimeSpan(0, 5, 0);
        public TimeSpan EmailVerificationCachePeriod => new TimeSpan(0, 5, 0);
    }
}
