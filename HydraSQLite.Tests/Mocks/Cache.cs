﻿using System;
using System.Collections.Concurrent;
using HydraSQLite.Services.Mockable;

namespace Tests.Mocks {

    public class Cache : ICache {

        private static ConcurrentDictionary<string, object> myDictionary;

        static Cache() {
            System.Diagnostics.Debug.WriteLine("*** CACHE static constructor");
            myDictionary = new ConcurrentDictionary<string, object>();
        }
        
        public bool Contains(string key) {
            return myDictionary.ContainsKey(key);
        }

        public object Get(string key) {
            System.Diagnostics.Debug.WriteLine($"*** CACHE get - contains {key} = {(Contains(key) ? "yes" : "NO")}");
            return myDictionary.ContainsKey(key) ? myDictionary[key] : null;
        }

        public void Remove(string key) {
            myDictionary.TryRemove(key, out object _);
        }

        public void Set(string key, object value, DateTimeOffset absoluteExpiration) {
            // ignore expiration
            Set(key, value);
        }

        // methods just for test context

        public void Set(string key, object value) {
            System.Diagnostics.Debug.WriteLine($"*** CACHE setting {key} = {value}");
            myDictionary[key] = value;
        }
        
        public void ResetAll() {
            System.Diagnostics.Debug.WriteLine("*** CACHE RESET");
            myDictionary = new ConcurrentDictionary<string, object>();
        }
    }
}
