﻿using System;

using HydraSQLite.Services;

namespace Tests.Mocks {

    public class CurrentUser : ICurrentUserIdentity {

        // make static so same for all tests (maybe should use cache?)
        private static Guid myUserID = Guid.Empty;

        public Guid UserID {
            get => myUserID;
            set => myUserID = value;
        }
    }
}
