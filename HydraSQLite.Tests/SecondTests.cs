﻿using Xunit;
using Tests.XUnitExtensions;
using Xunit.Abstractions;

namespace Tests {

    [Sequence(2)]
    public class SecondTests : SequencedTest {

        private readonly ITestOutputHelper myOutput;

        public SecondTests(ITestOutputHelper output) {
            myOutput = output;
        }
        
        [Fact, Sequence(3)]
        public void AThird() {
            myOutput.WriteLine("Test 3");
            System.Diagnostics.Debug.WriteLine("@@# Test 2/3");
            Assert.True(true);
        }

        [Fact, Sequence(2)]
        public void CSecond() {
            myOutput.WriteLine("Test 2");
            System.Diagnostics.Debug.WriteLine("@@# Test 2/2");
            Assert.True(true);
        }

        [Fact, Sequence(1)]
        public void BFirst() {
            myOutput.WriteLine("Test 1");
            System.Diagnostics.Debug.WriteLine("@@# Test 2/1");
            Assert.True(true);
        }
        
    }
}
