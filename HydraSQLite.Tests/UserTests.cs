﻿using Xunit;
using Tests.XUnitExtensions;
using Xunit.Abstractions;
using System;
using System.Linq;
using System.Threading.Tasks;
using HydraSQLite.Model;
using HydraSQLite.Model.Api;
using HydraSQLite.Services;
using HydraSQLite.Services.Mockable;
using Moq;

namespace Tests {

    [Sequence(1)]
    public class UserTests : SequencedTest, IDisposable, IClassFixture<SharedServicesFixture> {

        private readonly ITestOutputHelper myOutput;
        private readonly SharedServicesFixture myServices;

        private const string VerificationEmailCacheKey = "verificationEmailKey";

        public UserTests(ITestOutputHelper output, SharedServicesFixture sharedServices) {
            myServices = sharedServices;
            myOutput = output;
        }

        [Fact, Sequence(1)]
        public async Task CreateUser() {
            System.Diagnostics.Debug.WriteLine("--- UserTests CreateUser");
            // arrange
            var captcha = new CaptchaImage();
            myServices.Cache.Set(captcha.UniqueId, captcha, DateTimeOffset.Now.AddMinutes(1));           
            var userToCreate = new CreateAccountRequest {
                Email = Services.RandomText.Email(),
                Forename = Services.RandomText.Name(),
                Surname = Services.RandomText.Name(),
                Password = Services.RandomText.Get(8),
                CaptchaText = captcha.Text,
                CaptchaID = captcha.UniqueId
            };
            var createAccountService = new CreateAccount {
                Config = myServices.Config, 
                Emailer = myServices.Emailer, 
                Cache = myServices.Cache, 
                EmailUrlKey = myServices.EmailUrlKey, 
                UserRepository = myServices.UserRepository, 
                UserLogic = new HydraSQLite.Services.BusinessLogic.UserLogic {
                    Login = myServices.Login
                },
                PasswordLogic = new HydraSQLite.Services.BusinessLogic.PasswordLogic {
                    CommonPasswordRepository = new CommonPasswordRepository(myServices.Config, myServices.Cache)
                }
            };
            var url = Mock.Of<IUrlHelperWebApi>();
            myOutput.WriteLine($"Creating user email = '{userToCreate.Email}'...");
            myServices.Cache.Set("newUserEmail", userToCreate.Email); // save for later

            // act
            var response = await createAccountService.StartCreateAccount(url, userToCreate);
            // we could possibly fake an email service here to test email verification. meh...
            myServices.Cache.Set(VerificationEmailCacheKey, response.EmailUrlKey); // save for later

            // assert
            Assert.False(response.EmailUrlKey == null, "Expected email-URL key but found null");
            Assert.False(response.CaptchaWrong, "Capcha code was wrong");
            Assert.False(response.CaptchaExpired, "Capcha code expired");
            Assert.True(response.Status == UserStatus.WaitingEmailVerification, $"Expected status of WaitingEmailVerification but found: {response.Status}");
        }

        [Fact, Sequence(2)]
        public void VerifyEmail() {
            System.Diagnostics.Debug.WriteLine("--- UserTests VerifyEmail");
            // arrange
            var createAccountService = new CreateAccount {
                Config = myServices.Config, 
                Emailer = myServices.Emailer, 
                Cache = myServices.Cache, 
                EmailUrlKey = myServices.EmailUrlKey, 
                UserRepository = myServices.UserRepository, 
                UserLogic = new HydraSQLite.Services.BusinessLogic.UserLogic {
                    Login = myServices.Login
                },
                PasswordLogic = new HydraSQLite.Services.BusinessLogic.PasswordLogic {
                    CommonPasswordRepository = new CommonPasswordRepository(myServices.Config, myServices.Cache)
                }
            };
            var key = myServices.Cache.Get(VerificationEmailCacheKey) as string;

            // act
            var result = createAccountService.VerifyEmail(key);

            // assert
            Assert.True(key != null, "Key for email URL lookup was null");
            Assert.True(result == VerifyEmailResult.Success, $"Unexpected verification status: {result}");
        }

        [Fact, Sequence(3)]
        public void Login() {
            System.Diagnostics.Debug.WriteLine("--- UserTests Login");
            // arrange
            var loginService = new Login(myServices.Config, myServices.Emailer, myServices.Cache, myServices.EmailUrlKey, myServices.UserRepository, myServices.CurrentUser);
            var url = Mock.Of<IUrlHelperMvc>();
            var formsAuth = Mock.Of<IFormsAuthentication>();
            const string email = "mark.hodgson@breeze-it.com";
            var loginRequest = new LoginRequest {
                Email = email,
                Password = "breeze**",
                ClientNOnce = Services.RandomText.Get(8)
            };

            // act
            LoginResponse response;
            if (myServices.Config.IsDigestAuthenticationScheme) {
                var serverNOnce = loginService.CreateServerNOnce(email);
                loginRequest.Digest = Crypto.GetHash(serverNOnce + loginRequest.ClientNOnce + loginRequest.Password);
                loginRequest.Password = "";
                response = loginService.CheckDigest(url, formsAuth, loginRequest);
            }
            else {
                response = loginService.CheckEmailAndPassword(url, formsAuth, loginRequest);
            }
            if (response.Message == "ok") {
                // used by user repo to get logged in user
                myServices.CurrentUser.UserID = response.User.ID;
            }

            // assert
            Assert.True(response.Message == "ok", $"Unexpected response text = '{response.Message}'");
            Assert.True(response.User.ID != Guid.Empty, "Expected non-empty user id.");
        }

        [Fact, Sequence(4)]
        public void Approve() {
            System.Diagnostics.Debug.WriteLine("--- UserTests Approve");
            // arrange
            var email = myServices.Cache.Get("newUserEmail") as string;
            var user = myServices.UserRepository.Search(new UserSearchParameters {
                Email = email,
                Statuses = new [] { UserStatus.WaitingApproval }
            }).FirstOrDefault();
            var needApprovalCountBefore = myServices.UserRepository.GetAwaitingApprovalCount();
            
            // act
            if (user != null) {
                user.Status = UserStatus.Active;
                myServices.UserRepository.Save(user);
            }
            var needApprovalCountAfter = myServices.UserRepository.GetAwaitingApprovalCount();

            // assert
            Assert.True(user != null, $"User with email = '{email}' was null.");
            Assert.True(needApprovalCountBefore > needApprovalCountAfter, $"User count before  ({needApprovalCountAfter}) should be greater than after ({needApprovalCountAfter})");
        }

        // this is a clean-up as much as a test
        [Fact, Sequence(5)]
        public void Delete() {
            System.Diagnostics.Debug.WriteLine("--- UserTests Delete");
            // arrange
            var email = myServices.Cache.Get("newUserEmail") as string;
            var user = myServices.UserRepository.Search(new UserSearchParameters {
                Email = email
            }).FirstOrDefault();

            // act
            myServices.UserRepository.Delete(user);
            user = myServices.UserRepository.Search(new UserSearchParameters {
                Email = email
            }).FirstOrDefault();

            // asset
            Assert.True(user == null, "User should have been null after deletion.");
        }

        public void Dispose() {
            System.Diagnostics.Debug.WriteLine("@@@ UserTests Dispose");
        }
    }
}
