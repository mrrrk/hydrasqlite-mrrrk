﻿using System;

namespace Tests.Services {

    // NB - this is NOT crytographically strong - but will do for testing
    // (Do not use this code for creating live security-sensitive random strings)
    static class RandomText {

        private static readonly Random myRandom;

        static RandomText() {
            myRandom = new Random(DateTime.Now.Millisecond);
        }

        public static string Get(int length) {
            var rnd = new Random(DateTime.Now.Millisecond);
            var tokenChars = new char[length];
            for (var i = 0; i < tokenChars.Length; i++) {
                var pos = rnd.Next(0, myAvailableTokenCharacters.Length - 1);
                tokenChars[i] = myAvailableTokenCharacters[pos];
            }
            return new string(tokenChars);
        }

        public static string Name() {
            var rnd = new Random(DateTime.Now.Millisecond);
            var len = rnd.Next(3, 8);
            return $"{GetUppercase(1)}{GetLowercase(len)}";
        }

        public static string Email() {
            return $"{GetLowercase(5)}.{GetLowercase(5)}@{GetLowercase(7)}.co.uk";
        }

        public static string GetUppercase(int length) {
            var rnd = new Random(DateTime.Now.Millisecond);
            var tokenChars = new char[length];
            for (var i = 0; i < tokenChars.Length; i++) {
                var pos = rnd.Next(0, 25);
                tokenChars[i] = myAvailableTokenCharacters[pos];
            }
            return new string(tokenChars);
        }

        public static string GetLowercase(int length) {
            var tokenChars = new char[length];
            for (var i = 0; i < tokenChars.Length; i++) {
                var pos = myRandom.Next(26, 51);
                tokenChars[i] = myAvailableTokenCharacters[pos];
            }
            return new string(tokenChars);
        }


        private static readonly char[] myAvailableTokenCharacters = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };
    }
}
